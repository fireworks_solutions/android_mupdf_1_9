package com.artifex.mupdflib;

import com.artifex.mupdfdemo.R;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class SimpleWebView extends Activity {
	public static final String URL_KEY = "url";
	private WebView webView;

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (webView.canGoBack()) {
			webView.goBack();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview_in_app);
		getActionBar().hide();
		ProgressBar progressBar = (ProgressBar) findViewById(R.id.webview_loading_indi);
		webView = (WebView) findViewById(R.id.mupdf_webview);

		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new SpecialWebview(progressBar));
		webView.setWebChromeClient(new SpecialWebChromeClient(progressBar));

		String URL = getIntent().getStringExtra(URL_KEY);
		webView.loadUrl(URL);
	}

	public class SpecialWebChromeClient extends WebChromeClient {
		ProgressBar progressBar;

		public SpecialWebChromeClient(ProgressBar progressBar) {
			// TODO Auto-generated constructor stub
			this.progressBar = progressBar;
		}

		@Override
		public void onProgressChanged(WebView view, int newProgress) {
			// TODO Auto-generated method stub
			progressBar.setProgress(newProgress);
		}

	}

	public class SpecialWebview extends WebViewClient {
		private ProgressBar progressBar;

		public SpecialWebview(ProgressBar progressBar) {
			this.progressBar = progressBar;
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// TODO Auto-generated method stub
			progressBar.setVisibility(View.VISIBLE);
			progressBar.setProgress(0);
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub

			super.onPageFinished(view, url);
			progressBar.setVisibility(View.GONE);
		}

	}
}
