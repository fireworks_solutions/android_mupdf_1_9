package com.artifex.mupdflib;

import android.os.Bundle;

public class InteractiveData {

	private int interX1;
	private int interY1;
	private int interX2;
	private int interY2;
	private String interType;
	private String interUrl, interEmailSubject, buttonSource;
	private boolean interAutoPlay, loop, openInBrowser;

	private Bundle interactiveData;

	public static final String INTER_URL_KEY = "interUrl";
	public static final String INTER_EMAIL_SUBJECT_KEY = "interEmailSubject";
	public static final String INTER_AUTO_PLAY = "interAutoPlay";
	public static final String INTER_LOOP = "loop";
	public static final String INTER_OPEN_IN_BLANK = "_blank";

	public InteractiveData(int interX1, int interY1, int interX2, int interY2,
			String interType, String openInBrowser, Bundle interUrl) {

		this.interX1 = interX1;
		this.interY1 = interY1;
		this.interX2 = interX2;
		this.interY2 = interY2;
		this.interType = interType;
		if (interUrl != null) {
			this.interUrl = interUrl.getString(INTER_URL_KEY);
			this.interEmailSubject = interUrl
					.getString(INTER_EMAIL_SUBJECT_KEY);
			interAutoPlay = interUrl.containsKey(INTER_AUTO_PLAY)
					&& interUrl.getString(INTER_AUTO_PLAY) != null ? interUrl
					.getString(INTER_AUTO_PLAY).equals("autoplay") : false;
			loop = interUrl.containsKey(INTER_LOOP)
					&& interUrl.getString(INTER_LOOP) != null ? interUrl
					.getString(INTER_LOOP).equals("loop") : false;
			interactiveData = interUrl;
		}
		setOpenInBrowser(openInBrowser);
	}

	public InteractiveData() {
	}

	public int getInterX1() {
		return interX1;
	}

	public void setInterX1(int interX1) {
		this.interX1 = interX1;
	}

	public int getInterY1() {
		return interY1;
	}

	public void setInterY1(int interY1) {
		this.interY1 = interY1;
	}

	public int getInterX2() {
		return interX2;
	}

	public void setInterX2(int interX2) {
		this.interX2 = interX2;
	}

	public int getInterY2() {
		return interY2;
	}

	public void setInterY2(int interY2) {
		this.interY2 = interY2;
	}

	public void setButtonSource(String buttonSource) {
		this.buttonSource = buttonSource;
	}

	public String getButtonSource() {
		return buttonSource;
	}

	public String getInterType() {
		return interType;
	}

	public void setInterType(String interType) {
		this.interType = interType;
	}

	public String getInterUrl() {
		return interUrl;
	}

	public String getInterEmailSubject() {
		return interEmailSubject;
	}

	public boolean getIsAutoPlay() {
		// TODO Auto-generated method stub
		return interAutoPlay;
	}

	public boolean getIsLoop() {
		// TODO Auto-generated method stub
		return loop;
	}

	public Bundle getInteractiveData() {
		return interactiveData;
	}

	public void setOpenInBrowser(String openInBrowser) {
		this.openInBrowser = openInBrowser.equals(INTER_OPEN_IN_BLANK);
	}

	public boolean getOpenInBrowser() {
		return openInBrowser;
	}

	public void setInteractiveData(Bundle interactiveData) {
		this.interUrl = interactiveData.getString(INTER_URL_KEY);
		this.interEmailSubject = interactiveData
				.getString(INTER_EMAIL_SUBJECT_KEY);
		interAutoPlay = interactiveData.containsKey(INTER_AUTO_PLAY)
				&& interactiveData.getString(INTER_AUTO_PLAY) != null ? interactiveData
				.getString(INTER_AUTO_PLAY).equals("autoplay") : false;
		loop = interactiveData.containsKey(INTER_LOOP)
				&& interactiveData.getString(INTER_LOOP) != null ? interactiveData
				.getString(INTER_LOOP).equals("loop") : false;
		this.interactiveData = interactiveData;
	}
	// public void setInterUrl(String interUrl) {
	// this.interUrl = interUrl;
	// }
}