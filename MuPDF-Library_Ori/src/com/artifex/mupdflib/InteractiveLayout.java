package com.artifex.mupdflib;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

/**
 * Created by fireworks on 5/25/15.
 */

public class InteractiveLayout extends RelativeLayout {
	Context mContext;

	public InteractiveLayout(Context context) {
		super(context);
		mContext = context;
	}

	public void onCustomTouch(MotionEvent event) {
	}
}