package com.artifex.mupdflib;
//package com.artifex.mupdfdemo;
//
//import android.annotation.SuppressLint;
//import android.app.AlertDialog;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.*;
//import android.graphics.Bitmap.Config;
//import android.media.MediaPlayer;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Handler;
//import android.util.Log;
//import android.view.GestureDetector;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.WindowManager;
//import android.webkit.WebChromeClient;
//import android.webkit.WebSettings;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.ProgressBar;
//import android.widget.RelativeLayout;
//import android.widget.Toast;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Iterator;
//
///*
//class PatchInfo {
//	//public BitmapHolder bmh;
//	//public Bitmap bm;
//	public Point patchViewSize;
//	public Rect patchArea;
//	public boolean completeRedraw;
//
//	public PatchInfo(Point aPatchViewSize, Rect aPatchArea, boolean aCompleteRedraw) {
//		//bmh = aBmh;
//		//bm = null;
//		patchViewSize = aPatchViewSize;
//		patchArea = aPatchArea;
//		completeRedraw = aCompleteRedraw;
//	}
//}
//*/
//// Make our ImageViews opaque to optimize redraw
//class OpaqueImageView extends ImageView {
//
//    public OpaqueImageView(Context context) {
//        super(context);
//    }
//
//    @Override
//    public boolean isOpaque() {
//        return true;
//    }
//}
//
//interface TextProcessor {
//    void onStartLine();
//
//    void onWord(TextWord word);
//
//    void onEndLine();
//}
//
//class TextSelector {
//    final private TextWord[][] mText;
//    final private RectF mSelectBox;
//
//    public TextSelector(TextWord[][] text, RectF selectBox) {
//        mText = text;
//        mSelectBox = selectBox;
//    }
//
//    public void select(TextProcessor tp) {
//        if (mText == null || mSelectBox == null)
//            return;
//
//        ArrayList<TextWord[]> lines = new ArrayList<TextWord[]>();
//        for (TextWord[] line : mText)
//            if (line[0].bottom > mSelectBox.top && line[0].top < mSelectBox.bottom)
//                lines.add(line);
//
//        Iterator<TextWord[]> it = lines.iterator();
//        while (it.hasNext()) {
//            TextWord[] line = it.next();
//            boolean firstLine = line[0].top < mSelectBox.top;
//            boolean lastLine = line[0].bottom > mSelectBox.bottom;
//            float start = Float.NEGATIVE_INFINITY;
//            float end = Float.POSITIVE_INFINITY;
//
//            if (firstLine && lastLine) {
//                start = Math.min(mSelectBox.left, mSelectBox.right);
//                end = Math.max(mSelectBox.left, mSelectBox.right);
//            } else if (firstLine) {
//                start = mSelectBox.left;
//            } else if (lastLine) {
//                end = mSelectBox.right;
//            }
//
//            tp.onStartLine();
//
//            for (TextWord word : line)
//                if (word.right > start && word.left < end)
//                    tp.onWord(word);
//
//            tp.onEndLine();
//        }
//    }
//}
//
//public abstract class PageView extends ViewGroup {
//    private static final int HIGHLIGHT_COLOR = 0x802572AC;
//    private static final int LINK_COLOR = 0x20AC7225;
//    private static final int BOX_COLOR = 0xFF4444FF;
//    private static final int INK_COLOR = 0xFFFFFFFF;
//    private static final float INK_THICKNESS = 10.0f;
//    private static final int BACKGROUND_COLOR = 0xFFFFFFFF;
//    private static final int PROGRESS_DIALOG_DELAY = 200;
//    private static final String TAG = "PageView";
//
//    protected final Context mContext;
//    protected int mPageNumber;
//    private Point mParentSize;
//    protected Point mSize; // Size of page at minimum zoom
//    protected float mSourceScale;
//
//    private ImageView mEntire; // Image rendered at minimum zoom
//    //private BitmapHolder mEntireBmh;
//    private Bitmap mEntireBm;
//    private Matrix mEntireMat;
//    private AsyncTask<Void, Void, TextWord[][]> mGetText;
//    private AsyncTask<Void, Void, LinkInfo[]> mGetLinkInfo;
//    //private AsyncTask<Void, Void, Bitmap> mDrawEntire;
//    //private       AsyncTask<Void,Void,Void> mDrawEntire;
//    private CancellableAsyncTask<Void, Void> mDrawEntire;
//
//
//    private Point mPatchViewSize; // View size on the basis of which the patch
//    // was created
//    private Rect mPatchArea;
//    private ImageView mPatch;
//    //private BitmapHolder mPatchBmh;
//    private Bitmap mPatchBm;
//    //private AsyncTask<PatchInfo, Void, PatchInfo> mDrawPatch;
//    private CancellableAsyncTask<Void, Void> mDrawPatch;
//
//    private RectF mSearchBoxes[];
//    private RectF mSearchBoxesPrim[];
//    protected LinkInfo mLinks[];
//    private RectF mSelectBox;
//    private TextWord mText[][];
//    private RectF mItemSelectBox;
//    protected ArrayList<ArrayList<PointF>> mDrawing;
//    private View mSearchView;
//    private boolean mIsBlank;
//    private boolean mHighlightLinks;
//
//    private ProgressBar mBusyIndicator;
//    ProgressDialog mDialog;
//
//    private final Handler mHandler = new Handler();
//
//
//    private RelativeLayout[] interactiveLayers;
//    private RelativeLayout[] interactiveLayers2;
//    private boolean parentChecker = false;
//    private boolean childChecker = false;
//    MuPDFCore mCore;
//    private InteractiveData[] interactiveData;
//    private InteractiveData[] interactiveData2;
//    private boolean interactiveEnabled = false;
//
//    //This is hardcoded page height and width, this is used to calculate the correct position of the interactive
//    private int hardCodedPageHeight = 1024;
//    private int hardCodedPageWidth = 768;
//    public MediaPlayer mMediaPlayer = null;
//    public boolean isPlay = false;
//
//
//    //public PageView(Context c, Point parentSize) {
//    public PageView(Context c, Point parentSize, Bitmap sharedHqBm, MuPDFCore core) {
//
//        super(c);
//        mCore = core;
//        mContext = c;
//        mParentSize = parentSize;
//        setBackgroundColor(BACKGROUND_COLOR);
//        //mEntireBmh = new BitmapHolder();
//        //mPatchBmh = new BitmapHolder();
//        try {
//            mEntireBm = Bitmap.createBitmap(parentSize.x, parentSize.y, Config.ARGB_8888);
//        } catch (OutOfMemoryError e) {
//            Log.d("MY_OOM_ERROR", "error");
//        }
//
//        mPatchBm = sharedHqBm;
//        mEntireMat = new Matrix();
//        mDialog = new ProgressDialog(mContext);
//        mDialog.setMessage("Please wait...");
//        mDialog.setCancelable(false);
////        setOnTouchListener(new OnTouchListener() {
////            @Override
////            public boolean onTouch(View v, MotionEvent event) {
////                System.out.println("Kelvyn:pageviewTouched");
////                ((ReaderView)getParent()).scrollBy(50,0);
////                return true;
////            }
////        });
//
//    }
//
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        System.out.println("Kelvyn:interactiveEnabled->" + interactiveEnabled);
//
//        if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP && interactiveEnabled) {
//            for (int i = 0; i < this.getChildCount(); i++) {
//                System.out.println("this.getChildAt(" + i + ")->" + this.getChildAt(i));
//                this.getChildAt(i).dispatchTouchEvent(event);
//            }
//        }
//        return false;
//    }
//
//    //protected abstract Bitmap drawPage(int sizeX, int sizeY, int patchX,
//    //		int patchY, int patchWidth, int patchHeight);
//
//    //protected abstract Bitmap updatePage(BitmapHolder h, int sizeX, int sizeY,
//    //		int patchX, int patchY, int patchWidth, int patchHeight);
//    //protected abstract void drawPage(Bitmap bm, int sizeX, int sizeY, int patchX, int patchY, int patchWidth, int patchHeight);
//    //protected abstract void updatePage(Bitmap bm, int sizeX, int sizeY, int patchX, int patchY, int patchWidth, int patchHeight);
//    protected abstract CancellableTaskDefinition<Void, Void> getDrawPageTask(Bitmap bm, int sizeX, int sizeY, int patchX, int patchY, int patchWidth, int patchHeight);
//
//    protected abstract CancellableTaskDefinition<Void, Void> getUpdatePageTask(Bitmap bm, int sizeX, int sizeY, int patchX, int patchY, int patchWidth, int patchHeight);
//
//    protected abstract LinkInfo[] getLinkInfo();
//
//    protected abstract TextWord[][] getText();
//
//    protected abstract void addMarkup(PointF[] quadPoints, Annotation.Type type);
//
//    private void reinit() {
//        // Cancel pending render task
//        if (mDrawEntire != null) {
//            //mDrawEntire.cancel(true);
//            mDrawEntire.cancelAndWait();
//            mDrawEntire = null;
//        }
//
//        if (mDrawPatch != null) {
//            //mDrawPatch.cancel(true);
//            mDrawPatch.cancelAndWait();
//            mDrawPatch = null;
//        }
//
//        if (mGetLinkInfo != null) {
//            mGetLinkInfo.cancel(true);
//            mGetLinkInfo = null;
//        }
//
//        if (mGetText != null) {
//            mGetText.cancel(true);
//            mGetText = null;
//        }
//
//        mIsBlank = true;
//        mPageNumber = 0;
//
//        if (mSize == null)
//            mSize = mParentSize;
//
//        if (mEntire != null) {
//            mEntire.setImageBitmap(null);
//            //mEntireBmh.setBm(null);
//            mEntire.invalidate();
//        }
//
//        if (mPatch != null) {
//            mPatch.setImageBitmap(null);
//            //mPatchBmh.setBm(null);
//            mPatch.invalidate();
//        }
//
//        mPatchViewSize = null;
//        mPatchArea = null;
//
//        mSearchBoxes = null;
//        mSearchBoxesPrim = null;
//        mLinks = null;
//        mSelectBox = null;
//        mText = null;
//        mItemSelectBox = null;
//    }
//
//    public void releaseResources() {
//        reinit();
//
//        if (mBusyIndicator != null) {
//            mDialog.dismiss();
//            removeView(mBusyIndicator);
//            mBusyIndicator = null;
//        }
//    }
//
//    public void releaseBitmaps() {
//        reinit();
//        mEntireBm = null;
//        mPatchBm = null;
//    }
//
//    public void blank(int page) {
//        reinit();
//        mPageNumber = page;
//
//        if (mBusyIndicator == null) {
////            if (((MuPDFReaderView) getParent()) != null) {
////                if (page == ((MuPDFReaderView) getParent()).getDisplayedViewIndex()) {
////                    mDialog.show();
////                }
////            }
//            View view = new View(mContext);
//            view.setLayoutParams(new LayoutParams(10000,10000));
//            view.setBackgroundResource(R.drawable.busy);
//            addView(view);
//            mBusyIndicator = new ProgressBar(mContext);
//            mBusyIndicator.setIndeterminate(true);
//            mBusyIndicator.setBackgroundResource(R.drawable.busy);
//            addView(mBusyIndicator);
//        }
//        setBackgroundColor(BACKGROUND_COLOR);
//    }
//
//    public void setPage(final int page, PointF size) {
//
//        System.out.println("Kelvyn:setPage");
//
//        // Cancel pending render task
//        if (mDrawEntire != null) {
//            System.out.println("Kelvyn:mDrawEntire is not null");
//            //mDrawEntire.cancel(true);
//            mDrawEntire.cancelAndWait();
//            mDrawEntire = null;
//        }
//
//        mIsBlank = false;
//        // Highlights may be missing because mIsBlank was true on last draw
//        if (mSearchView != null)
//            mSearchView.invalidate();
//
//        mPageNumber = page;
//        if (mEntire == null) {
//            mEntire = new OpaqueImageView(mContext);
//            //mEntire.setScaleType(ImageView.ScaleType.FIT_CENTER);
//            mEntire.setScaleType(ImageView.ScaleType.MATRIX);
//            addView(mEntire);
//        }
//
//        interactiveLayers = null;
//        interactiveLayers2 = null;
//        createInteractiveButtons();
//        enableInteractive();
//
////        ViewGroup.MarginLayoutParams params = new ViewGroup.MarginLayoutParams(500,500);
////        params.setMargins(50,50,30,30);
////        interactiveLayer.setLayoutParams(params);
//
//
//        System.out.println("Kelvyn:addedInteractiveLayer");
//
//        // Calculate scaled size that fits within the screen limits
//        // This is the size at minimum zoom
//        mSourceScale = Math.min(mParentSize.x / size.x, mParentSize.y / size.y);
//        Point newSize = new Point((int) (size.x * mSourceScale),
//                (int) (size.y * mSourceScale));
//        mSize = newSize;
//
//        mEntire.setImageBitmap(null);
//        //mEntireBmh.setBm(null);
//        mEntire.invalidate();
//
//        // Get the link info in the background
//        mGetLinkInfo = new AsyncTask<Void, Void, LinkInfo[]>() {
//            protected LinkInfo[] doInBackground(Void... v) {
//                return getLinkInfo();
//            }
//
//            protected void onPostExecute(LinkInfo[] v) {
//                mLinks = v;
//                //invalidate();
//                if (mSearchView != null)
//                    mSearchView.invalidate();
//
//            }
//        };
//
//        mGetLinkInfo.execute();
//
//        // Render the page in the background
//        //mDrawEntire = new AsyncTask<Void, Void, Void>() {
//        //	protected Void doInBackground(Void... v) {
//        //		drawPage(mEntireBm, mSize.x, mSize.y, 0, 0, mSize.x, mSize.y);
//        //		return null;
//        //	}
//        mDrawEntire = new CancellableAsyncTask<Void, Void>(getDrawPageTask(mEntireBm, mSize.x, mSize.y, 0, 0, mSize.x, mSize.y)) {
//
//            //protected void onPreExecute() {
//            @Override
//            public void onPreExecute() {
//                setBackgroundColor(BACKGROUND_COLOR);
//                mEntire.setImageBitmap(null);
//                //mEntireBmh.setBm(null);
//                mEntire.invalidate();
//
//                if (mBusyIndicator == null) {
//                    if (((MuPDFReaderView) getParent()) != null) {
//                        if (page == ((MuPDFReaderView) getParent()).getDisplayedViewIndex()) {
//                            mDialog.show();
//                        }
//                    }
//                    View view = new View(mContext);
//                    view.setLayoutParams(new LayoutParams(10000,10000));
//                    view.setBackgroundResource(Color.BLACK);
//                    addView(view);
//                    mBusyIndicator = new ProgressBar(mContext);
//                    mBusyIndicator.setIndeterminate(true);
//                    mBusyIndicator.setBackgroundResource(R.drawable.busy);
//                    addView(mBusyIndicator);
//                    mBusyIndicator.setVisibility(INVISIBLE);
//                    mHandler.postDelayed(new Runnable() {
//                        public void run() {
//                            if (mBusyIndicator != null)
//                                mBusyIndicator.setVisibility(VISIBLE);
//                        }
//                    }, PROGRESS_DIALOG_DELAY);
//                }
//            }
//
//            //protected void onPostExecute(Void v) {
//            @Override
//            public void onPostExecute(Void result) {
//                mDialog.dismiss();
//                removeView(mBusyIndicator);
//                mBusyIndicator = null;
//                //mEntire.setImageBitmap(bm);
//                //mEntireBmh.setBm(bm);
//                mEntire.setImageBitmap(mEntireBm);
//                mEntire.invalidate();
//                //invalidate();
//                setBackgroundColor(Color.TRANSPARENT);
//            }
//        };
//
//        mDrawEntire.execute();
//
//        if (mSearchView == null) {
//            mSearchView = new View(mContext) {
//                @SuppressLint("DrawAllocation")
//                @Override
//                protected void onDraw(final Canvas canvas) {
//                    super.onDraw(canvas);
//                    // Work out current total scale factor
//                    // from source to view
//                    final float scale = mSourceScale * (float) getWidth() / (float) mSize.x;
//                    final Paint paint = new Paint();
//
//                    if (!mIsBlank && mSearchBoxes != null) {
//                        paint.setColor(HIGHLIGHT_COLOR);
//                        for (RectF rect : mSearchBoxes)
//                            canvas.drawRect(rect.left * scale,
//                                    rect.top * scale, rect.right * scale,
//                                    rect.bottom * scale, paint);
//                    }
//
//                    if (!mIsBlank && mSearchBoxesPrim != null) {
//                        paint.setColor(HIGHLIGHT_COLOR);
//                        for (RectF rect : mSearchBoxesPrim)
//                            canvas.drawRect(rect.left * scale + getWidth() / 2, rect.top * scale,
//                                    rect.right * scale + getWidth() / 2, rect.bottom * scale, paint);
//                    }
//
//                    if (!mIsBlank && mLinks != null && mHighlightLinks) {
//                        paint.setStrokeWidth(2);
//                        for (LinkInfo link : mLinks) {
//                            //canvas.drawRect(link.rect.left * scale, link.rect.top * scale,
//                            //		link.rect.right * scale, link.rect.bottom * scale, paint);
//                            RectF rectfa = new RectF((link.rect.left - 2) * scale, (link.rect.top - 2) * scale,
//                                    (link.rect.right + 2) * scale, (link.rect.bottom + 2) * scale);
//                            paint.setStyle(Paint.Style.FILL);
//                            paint.setColor(LINK_COLOR);
//                            canvas.drawRoundRect(rectfa, 3 * scale, 3 * scale, paint);
//
//                            paint.setStyle(Paint.Style.STROKE);
//                            paint.setColor(HIGHLIGHT_COLOR);
//                            canvas.drawRoundRect(rectfa, 3 * scale, 3 * scale, paint);
//                        }
//                    }
//
//                    if (mSelectBox != null && mText != null) {
//                        paint.setColor(HIGHLIGHT_COLOR);
//                        processSelectedText(new TextProcessor() {
//                            RectF rect;
//
//                            public void onStartLine() {
//                                rect = new RectF();
//                            }
//
//                            public void onWord(TextWord word) {
//                                rect.union(word);
//                            }
//
//                            public void onEndLine() {
//                                if (!rect.isEmpty())
//                                    canvas.drawRect(rect.left * scale, rect.top
//                                                    * scale, rect.right * scale,
//                                            rect.bottom * scale, paint);
//                            }
//                        });
//                    }
//
//                    if (mItemSelectBox != null) {
//                        paint.setStyle(Paint.Style.STROKE);
//                        paint.setColor(BOX_COLOR);
//                        canvas.drawRect(mItemSelectBox.left * scale,
//                                mItemSelectBox.top * scale,
//                                mItemSelectBox.right * scale,
//                                mItemSelectBox.bottom * scale, paint);
//                    }
//
//                    if (mDrawing != null) {
//                        Path path = new Path();
//                        PointF p;
//
//                        paint.setAntiAlias(true);
//                        paint.setDither(true);
//                        paint.setStrokeJoin(Paint.Join.ROUND);
//                        paint.setStrokeCap(Paint.Cap.ROUND);
//
//                        paint.setStyle(Paint.Style.FILL);
//                        paint.setStrokeWidth(INK_THICKNESS * scale);
//                        paint.setColor(INK_COLOR);
//
//                        Iterator<ArrayList<PointF>> it = mDrawing.iterator();
//                        while (it.hasNext()) {
//                            ArrayList<PointF> arc = it.next();
//                            if (arc.size() >= 2) {
//                                Iterator<PointF> iit = arc.iterator();
//                                p = iit.next();
//                                float mX = p.x * scale;
//                                float mY = p.y * scale;
//                                path.moveTo(mX, mY);
//                                while (iit.hasNext()) {
//                                    p = iit.next();
//                                    float x = p.x * scale;
//                                    float y = p.y * scale;
//                                    path.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
//                                    mX = x;
//                                    mY = y;
//                                }
//                                path.lineTo(mX, mY);
//                            } else {
//                                p = arc.get(0);
//                                canvas.drawCircle(p.x * scale, p.y * scale, INK_THICKNESS * scale / 2, paint);
//                            }
//                        }
//
//                        paint.setStyle(Paint.Style.STROKE);
//                        canvas.drawPath(path, paint);
//                    }
//                }
//            };
//
//            addView(mSearchView);
//        }
//        requestLayout();
//    }
//
//    public void setSearchBoxes(RectF searchBoxes[]) {
//        mSearchBoxes = searchBoxes;
//        if (mSearchView != null)
//            mSearchView.invalidate();
//    }
//
//    public void setSearchBoxesPrim(RectF searchBoxes[]) {
//        mSearchBoxesPrim = searchBoxes;
//        if (mSearchView != null)
//            mSearchView.invalidate();
//    }
//
//    public void setLinkHighlighting(boolean f) {
//        mHighlightLinks = f;
//        if (mSearchView != null)
//            mSearchView.invalidate();
//    }
//
//    public void deselectText() {
//        mSelectBox = null;
//        mSearchView.invalidate();
//    }
//
//    public void selectText(float x0, float y0, float x1, float y1) {
//        float scale = mSourceScale * (float) getWidth() / (float) mSize.x;
//        float docRelX0 = (x0 - getLeft()) / scale;
//        float docRelY0 = (y0 - getTop()) / scale;
//        float docRelX1 = (x1 - getLeft()) / scale;
//        float docRelY1 = (y1 - getTop()) / scale;
//        // Order on Y but maintain the point grouping
//        if (docRelY0 <= docRelY1)
//            mSelectBox = new RectF(docRelX0, docRelY0, docRelX1, docRelY1);
//        else
//            mSelectBox = new RectF(docRelX1, docRelY1, docRelX0, docRelY0);
//
//        mSearchView.invalidate();
//
//        if (mGetText == null) {
//            mGetText = new AsyncTask<Void, Void, TextWord[][]>() {
//                @Override
//                protected TextWord[][] doInBackground(Void... params) {
//                    return getText();
//                }
//
//                @Override
//                protected void onPostExecute(TextWord[][] result) {
//                    mText = result;
//                    mSearchView.invalidate();
//                }
//            };
//
//            mGetText.execute();
//        }
//    }
//
//    public void startDraw(float x, float y) {
//        float scale = mSourceScale * (float) getWidth() / (float) mSize.x;
//        float docRelX = (x - getLeft()) / scale;
//        float docRelY = (y - getTop()) / scale;
//        if (mDrawing == null)
//            mDrawing = new ArrayList<ArrayList<PointF>>();
//
//        ArrayList<PointF> arc = new ArrayList<PointF>();
//        arc.add(new PointF(docRelX, docRelY));
//        mDrawing.add(arc);
//        mSearchView.invalidate();
//    }
//
//    public void continueDraw(float x, float y) {
//        float scale = mSourceScale * (float) getWidth() / (float) mSize.x;
//        float docRelX = (x - getLeft()) / scale;
//        float docRelY = (y - getTop()) / scale;
//
//        if (mDrawing != null && mDrawing.size() > 0) {
//            ArrayList<PointF> arc = mDrawing.get(mDrawing.size() - 1);
//            arc.add(new PointF(docRelX, docRelY));
//            mSearchView.invalidate();
//        }
//    }
//
//    public void cancelDraw() {
//        mDrawing = null;
//        mSearchView.invalidate();
//    }
//
//    protected PointF[][] getDraw() {
//        if (mDrawing == null)
//            return null;
//
//        PointF[][] path = new PointF[mDrawing.size()][];
//
//        for (int i = 0; i < mDrawing.size(); i++) {
//            ArrayList<PointF> arc = mDrawing.get(i);
//            path[i] = arc.toArray(new PointF[arc.size()]);
//        }
//
//        return path;
//    }
//
//    protected void processSelectedText(TextProcessor tp) {
//        (new TextSelector(mText, mSelectBox)).select(tp);
//    }
//
//    public void setItemSelectBox(RectF rect) {
//        mItemSelectBox = rect;
//        if (mSearchView != null)
//            mSearchView.invalidate();
//    }
//
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        int x, y;
//        switch (MeasureSpec.getMode(widthMeasureSpec)) {
//            case MeasureSpec.UNSPECIFIED:
//                x = mSize.x;
//                break;
//            default:
//                x = MeasureSpec.getSize(widthMeasureSpec);
//        }
//        switch (MeasureSpec.getMode(heightMeasureSpec)) {
//            case MeasureSpec.UNSPECIFIED:
//                y = mSize.y;
//                break;
//            default:
//                y = MeasureSpec.getSize(heightMeasureSpec);
//        }
//
//        setMeasuredDimension(x, y);
//
//        if (mBusyIndicator != null) {
//            int limit = Math.min(mParentSize.x, mParentSize.y) / 2;
//            mBusyIndicator.measure(MeasureSpec.AT_MOST | limit,
//                    MeasureSpec.AT_MOST | limit);
//        }
//    }
//
//    @Override
//    protected void onLayout(boolean changed, int left, int top, int right,
//                            int bottom) {
//        int w = right - left;
//        int h = bottom - top;
//
//        if (mEntire != null) {
//            //mEntireMat.setScale(w/(float)mSize.x, h/(float)mSize.y);
//            //mEntire.setImageMatrix(mEntireMat);
//            //mEntire.invalidate();
//            if (mEntire.getWidth() != w || mEntire.getHeight() != h) {
//                mEntireMat.setScale(w / (float) mSize.x, h / (float) mSize.y);
//                mEntire.setImageMatrix(mEntireMat);
//                mEntire.invalidate();
//            }
//            mEntire.layout(0, 0, w, h);
//        }
//
//        if (mSearchView != null) {
//            mSearchView.layout(0, 0, w, h);
//        }
//
//        if (mPatchViewSize != null) {
//            if (mPatchViewSize.x != w || mPatchViewSize.y != h) {
//                // Zoomed since patch was created
//                mPatchViewSize = null;
//                mPatchArea = null;
//                if (mPatch != null) {
//                    mPatch.setImageBitmap(null);
//                    //mPatchBmh.setBm(null);
//                    mPatch.invalidate();
//                }
//            } else {
//                mPatch.layout(mPatchArea.left, mPatchArea.top,
//                        mPatchArea.right, mPatchArea.bottom);
//            }
//        }
//
//        if (interactiveLayers != null) {
//            System.out.println("Kelvyn:getWidth()-> " + getWidth() + " getHeight()->" + getHeight());
//            if (mPatchViewSize != null) {
//                System.out.println("Kelvyn:getWidth().x-> " + mPatchViewSize.x + " getHeight().y->" + mPatchViewSize.y);
//            }
//
//            float scale = ((ReaderView) getParent()).mScale;
//
//            double calculatedWidthRatio = (double) ((getWidth() / mCore.getDisplayPages() / scale) / hardCodedPageWidth);
//            double calculatedHeightRatio = (double) ((getHeight() / scale) / hardCodedPageHeight);
//            System.out.println("Kelvyn:calculatedWidthRatio mCore.getDisplayPages()-> " + mCore.getDisplayPages() + " calculatedHeightRatio->" + calculatedHeightRatio);
////            System.out.println("Kelvyn:getWidth().x-> " + mPatchViewSize.x + " getHeight().y->" + mPatchViewSize.y);
//            for (int i = 0; i < interactiveLayers.length; i++) {
//                if (interactiveLayers[i] != null) {
//                    System.out.println("548:getDisplayedViewIndex()->" + ((MuPDFReaderView) getParent()).getDisplayedViewIndex());
//                    System.out.println("548:mPageNumber->" + mPageNumber);
//                    int x1 = (int) (interactiveData[i].getInterX1() * calculatedWidthRatio * (w / (float) mSize.x));
//                    int x2 = (int) (interactiveData[i].getInterX2() * calculatedWidthRatio * (w / (float) mSize.x));
//                    int y1 = (int) (interactiveData[i].getInterY1() * calculatedHeightRatio * (h / (float) mSize.y));
//                    int y2 = (int) (interactiveData[i].getInterY2() * calculatedHeightRatio * (h / (float) mSize.y));
//                    interactiveLayers[i].layout(x1, y1, x2, y2);
//                }
//            }
//        }
//        if (mCore.getDisplayPages() == 2) {
//            if (interactiveLayers2 != null) {
//                float scale = ((ReaderView) getParent()).mScale;
//                double offset = getWidth() / mCore.getDisplayPages() / scale;
//                double calculatedWidthRatio = (double) ((getWidth() / mCore.getDisplayPages() / scale) / hardCodedPageWidth);
//                double calculatedHeightRatio = (double) ((getHeight() / scale) / hardCodedPageHeight);
//                System.out.println("Kelvyn:calculatedWidthRatio mCore.getDisplayPages()-> " + mCore.getDisplayPages() + " calculatedHeightRatio->" + calculatedHeightRatio);
//                System.out.println("Kelvyn:getWidth()2-> " + getWidth() + " getHeight()2->" + getHeight());
//                for (int i = 0; i < interactiveLayers2.length; i++) {
//                    if (interactiveLayers2[i] != null) {
//                        int x1 = (int) ((interactiveData2[i].getInterX1() * calculatedWidthRatio + offset) * (w / (float) mSize.x));
//                        int x2 = (int) ((interactiveData2[i].getInterX2() * calculatedWidthRatio + offset) * (w / (float) mSize.x));
//                        int y1 = (int) ((interactiveData2[i].getInterY1() * calculatedHeightRatio) * (h / (float) mSize.y));
//                        int y2 = (int) ((interactiveData2[i].getInterY2() * calculatedHeightRatio) * (h / (float) mSize.y));
//                        interactiveLayers2[i].layout(x1, y1, x2, y2);
//                    }
//                }
//            }
//        }
//
//        if (mBusyIndicator != null) {
//            int bw = mBusyIndicator.getMeasuredWidth();
//            int bh = mBusyIndicator.getMeasuredHeight();
//
//            mBusyIndicator.layout((w - bw) / 2, (h - bh) / 2, (w + bw) / 2,
//                    (h + bh) / 2);
//        }
//    }
//
//    public void updateHq(boolean update) {
//        System.out.println("Kelvyn:updateHQ");
//
//        Rect viewArea = new Rect(getLeft(), getTop(), getRight(), getBottom());
//        // If the viewArea's size matches the unzoomed size, there is no need
//        // for an hq patch
//        if (viewArea.width() == mSize.x || viewArea.height() == mSize.y) {
//            // If the viewArea's size matches the unzoomed size, there is no need for an hq patch
//            if (mPatch != null) {
//                mPatch.setImageBitmap(null);
//                mPatch.invalidate();
//            }
//        } else {
//            //Point patchViewSize = new Point(viewArea.width(), viewArea.height());
//            //Rect patchArea = new Rect(0, 0, mParentSize.x, mParentSize.y);
//            final Point patchViewSize = new Point(viewArea.width(), viewArea.height());
//            final Rect patchArea = new Rect(0, 0, mParentSize.x, mParentSize.y);
//
//            // Intersect and test that there is an intersection
//            if (!patchArea.intersect(viewArea))
//                return;
//
//            // Offset patch area to be relative to the view top left
//            patchArea.offset(-viewArea.left, -viewArea.top);
//
//            boolean area_unchanged = patchArea.equals(mPatchArea)
//                    && patchViewSize.equals(mPatchViewSize);
//
//            // If being asked for the same area as last time and not because of
//            // an update then nothing to do
//            if (area_unchanged && !update)
//                return;
//
//            boolean completeRedraw = !(area_unchanged && update);
//
//            // Stop the drawing of previous patch if still going
//            if (mDrawPatch != null) {
//                //mDrawPatch.cancel(true);
//                mDrawPatch.cancelAndWait();
//                mDrawPatch = null;
//            }
//
//            //if (completeRedraw) {
//            // The bitmap holder mPatchBm may still be rendered to by a
//            // previously invoked task, and possibly for a different
//            // area, so we cannot risk the bitmap generated by this task
//            // being passed to it
//            //	mPatchBmh.drop();
//            //	mPatchBmh = new BitmapHolder();
//            //}
//
//            // Create and add the image view if not already done
//            if (mPatch == null) {
//                mPatch = new OpaqueImageView(mContext);
//                //mPatch.setScaleType(ImageView.ScaleType.FIT_CENTER);
//                mPatch.setScaleType(ImageView.ScaleType.MATRIX);
//                addView(mPatch);
//
//                if (mSearchView != null)
//                    mSearchView.bringToFront();
//            }
//
//            if (interactiveLayers != null) {
//                System.out.println("Kelvyn:bringingtoFront interactiveLayers.length->" + interactiveLayers.length);
//                for (int i = 0; i < interactiveLayers.length; i++) {
//                    interactiveLayers[i].bringToFront();
//                }
//                if (mCore.getDisplayPages() == 2) {
//                    for (int i = 0; i < interactiveLayers2.length; i++) {
//                        interactiveLayers2[i].bringToFront();
//                    }
//                }
//            }
//
//            CancellableTaskDefinition<Void, Void> task;
//            if (completeRedraw)
//                task = getDrawPageTask(mPatchBm, patchViewSize.x, patchViewSize.y,
//                        patchArea.left, patchArea.top,
//                        patchArea.width(), patchArea.height());
//            else
//                task = getUpdatePageTask(mPatchBm, patchViewSize.x, patchViewSize.y,
//                        patchArea.left, patchArea.top,
//                        patchArea.width(), patchArea.height());
//
//            mDrawPatch = new CancellableAsyncTask<Void, Void>(task) {
//
//                public void onPostExecute(Void result) {
//                    mPatchViewSize = patchViewSize;
//                    mPatchArea = patchArea;
//
//                    mPatch.setImageBitmap(mPatchBm);
//                    mPatch.invalidate();
//                    mPatch.layout(mPatchArea.left, mPatchArea.top, mPatchArea.right, mPatchArea.bottom);
//                }
//            };
//
//            //mDrawPatch.execute(new PatchInfo(patchViewSize, patchArea, mPatchBmh, completeRedraw));
//            //mDrawPatch.execute(new PatchInfo(patchViewSize, patchArea, completeRedraw));
//            mDrawPatch.execute();
//
//
//        }
//    }
//
//    public void update() {
//        System.out.println("Kelvyn:update");
//        // Cancel pending render task
//        if (mDrawEntire != null) {
//            //mDrawEntire.cancel(true);
//            mDrawEntire.cancelAndWait();
//            mDrawEntire = null;
//        }
//
//        if (mDrawPatch != null) {
//            //mDrawPatch.cancel(true);
//            mDrawPatch.cancelAndWait();
//            mDrawPatch = null;
//        }
//
//        // Render the page in the background
//        mDrawEntire = new CancellableAsyncTask<Void, Void>(getUpdatePageTask(mEntireBm, mSize.x, mSize.y, 0, 0, mSize.x, mSize.y)) {
//            //mDrawEntire = new AsyncTask<Void, Void, Void>() {
//            //	protected Void doInBackground(Void... v) {
//            // Pass the current bitmap as a basis for the update, but use a
//            // bitmap
//            // holder so that the held bitmap will be nulled and not hold on
//            // to
//            // memory, should this view become redundant.
//            //		updatePage(mEntireBm, mSize.x, mSize.y, 0, 0, mSize.x, mSize.y);
//            //		return null;
//            //	}
//
//            //protected void onPostExecute(Bitmap bm) {
//            //	if (bm != null) {
//            //		mEntire.setImageBitmap(bm);
//            //		mEntireBmh.setBm(bm);
//            //	}
//            //	invalidate();
//            //protected void onPostExecute(Void v) {
//            public void onPostExecute(Void result) {
//                mEntire.setImageBitmap(mEntireBm);
//                mEntire.invalidate();
//
//            }
//        };
//
//        mDrawEntire.execute();
//
//        updateHq(true);
//    }
//
//    public void removeHq() {
//        // Stop the drawing of the patch if still going
//        if (mDrawPatch != null) {
//            //mDrawPatch.cancel(true);
//            mDrawPatch.cancelAndWait();
//            mDrawPatch = null;
//        }
//
//        // And get rid of it
//        mPatchViewSize = null;
//        mPatchArea = null;
//        if (mPatch != null) {
//            mPatch.setImageBitmap(null);
//            //mPatchBmh.setBm(null);
//            mPatch.invalidate();
//        }
//    }
//
//
//    public int getPage() {
//        return mPageNumber;
//    }
//
//    @Override
//    public boolean isOpaque() {
//        return true;
//    }
//
//
//    class MyGestureDetector implements GestureDetector.OnGestureListener {
//
//        @Override
//        public boolean onSingleTapUp(MotionEvent e) {
//            AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
//            builder1.setMessage("Interactive clicked");
//            builder1.setCancelable(true);
//            builder1.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.cancel();
//                }
//            });
//            AlertDialog alert11 = builder1.create();
//            alert11.show();
//            return false;
//        }
//
//
//        @Override
//        public void onShowPress(MotionEvent e) {
//
//        }
//
//        @Override
//        public void onLongPress(MotionEvent e) {
//
//        }
//
//        @Override
//        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
//            return false;
//        }
//
//        @Override
//        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
//            return false;
//        }
//
//        @Override
//        public boolean onDown(MotionEvent e) {
//            System.out.println("Kelvyn:DetectAction->" + e.getActionMasked());
////            if(e.getAction() == MotionEvent.ACTION_MOVE){
////                return false;
////            }else{
//            return true;
////            }
//
//        }
//    }
//
//    public void stopYoutube() {
//        ViewGroup rootView = (ViewGroup) this.getRootView().findViewWithTag("root");
////		RelativeLayout view = (RelativeLayout) rootView.findViewById(1001);
//        RelativeLayout view = (RelativeLayout) rootView.findViewWithTag("webview");
//        if (view != null) {
//            Log.d(TAG, "webview" + view);
////			WebView web = (WebView) view.findViewById(100);
//            WebView web = (WebView) view.findViewWithTag("youtube");
//            if (web != null) {
//                Log.d(TAG, "webview->Youtube " + web);
//                try {
//                    ((ViewGroup) web.getParent()).removeView(web);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                web.loadUrl("about:blank");
//                web.clearHistory();
//                web.clearCache(true);
//                web = null;
//            }
//        }
//    }
//
//    public WebView createYuotube() {
//        WebView youtube = new WebView(mContext);
////		youtube.setId(100);
//        youtube.setTag("youtube");
//        youtube.setWebChromeClient(new WebChromeClient());
//        youtube.getSettings().setJavaScriptEnabled(true);
////        youtube.getSettings().setPluginsEnabled(true);
//        youtube.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                return false;
//            }
//        });
//        ViewGroup rootView = (ViewGroup) this.getRootView().findViewWithTag("root");
//        int w = rootView.getWidth();
//        int h = rootView.getHeight();
//        int youtubeWidth;
//        int youtubeHeight;
//        if (w > h) {
//            youtubeWidth = (h * 3) / 2;
//            youtubeHeight = h;
//        } else {
//            youtubeWidth = w;
//            youtubeHeight = (w * 2) / 3;
//        }
//
//        RelativeLayout.LayoutParams youtubeLayoutParams = new RelativeLayout.LayoutParams(youtubeWidth, youtubeHeight);
//        youtubeLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
//        youtube.setLayoutParams(youtubeLayoutParams);
//
//        return youtube;
//    }
//
//    public void stopPlayer() {
//        if (mMediaPlayer != null) {
//            if (isPlay) {
//                mMediaPlayer.stop();
//                mMediaPlayer.release();
//                mMediaPlayer = null;
//                isPlay = false;
//            }
//        }
//    }
//
//    public void startIntent(String type, String data) {
//        if (type.equals("youtube")) {
//            Intent a = new Intent(getContext(), WebViewActivity.class);
//            a.putExtra("data", data);
//            getContext().startActivity(a);
//
////            ViewGroup rootView = (ViewGroup) this.getParent();
////            Log.d(TAG, "Root View " + rootView);
//////			RelativeLayout mBWebView = (RelativeLayout) rootView.findViewById(1001);
////            RelativeLayout mBWebView = (RelativeLayout) rootView.findViewWithTag("webview");
////            if (mBWebView != null) {
//////				WebView youtube = (WebView) mBWebView.findViewById(100);
////                WebView youtube = (WebView) mBWebView.findViewWithTag("youtube");
////                if (youtube != null) {
////                    Log.d(TAG, "webview->Youtube Exsist " + youtube);
////                    stopYoutube();
////                } else {
////                    Log.d(TAG, "webview->Youtube not " + youtube);
////                    youtube = createYuotube();
////                    youtube.loadUrl(data);
////                    mBWebView.addView(youtube);
////                }
////            } else {
////                mBWebView = new RelativeLayout(mContext);
//////				mBWebView.setId(1001);
////                mBWebView.setTag("webview");
////                WebView youtube = createYuotube();
////                youtube.loadUrl(data);
////                mBWebView.addView(youtube);
////                rootView.addView(mBWebView);
////            }
//        }
//        if (type.equals("a")) {
//            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data));
//            getContext().startActivity(browserIntent);
//        }
//        if (type.equals("email")) {
//            Intent emailIntent = new Intent(Intent.ACTION_SEND);
//            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{data});
//            emailIntent.setType("text/html");
//            Intent chooserIntent = Intent.createChooser(emailIntent, "Select email app");
//            getContext().startActivity(chooserIntent);
//        }
//        if (type.equals("audio")) {
//            if (mMediaPlayer != null) {
//                stopPlayer();
//            } else {
//                mMediaPlayer = new MediaPlayer();
//                try {
//                    int ind = data.lastIndexOf('/');
//                    String url = "file:///sdcard/" + getContext().getPackageName() + "/files/audio" + data.substring(ind);
//                    Log.d(TAG, url);
//                    Uri audioUri = Uri.parse(url);
//                    mMediaPlayer.setDataSource(getContext(), audioUri);
//                    mMediaPlayer.prepare();
//                    mMediaPlayer.start();
//                    isPlay = true;
//                } catch (IllegalArgumentException e) {
//                    e.printStackTrace();
//                } catch (SecurityException e) {
//                    e.printStackTrace();
//                } catch (IllegalStateException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        if (type.equals("video")) {
//            int ind = data.lastIndexOf('/');
//            String url = "file:///sdcard/" + getContext().getPackageName() + "/files/video" + data.substring(ind);
//            Toast.makeText(getContext(), url, Toast.LENGTH_SHORT).show();
//            Intent videoIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//            videoIntent.setDataAndType(Uri.parse(url), "video/mp4");
//            getContext().startActivity(videoIntent);
//        }
//    }
//
//    private void createInteractiveButtons() {
////        final GestureDetector gestureDetector = new GestureDetector(mContext, new MyGestureDetector());
//        if (mCore.getDisplayPages() == 2) {
//            if (mPageNumber != 0) {
//                interactiveData = mCore.getInteractive(mPageNumber * 2);
//            } else {
//                interactiveData = new InteractiveData[0];
//            }
//        } else {
//            interactiveData = mCore.getInteractive(mPageNumber + 1);
//        }
//        interactiveLayers = new InteractiveLayout[interactiveData.length];
//        System.out.println("Kelvyn:AddInteractiveLayer for mPageNumber-> " + mPageNumber + " length->" + interactiveData.length);
//
//        for (int i = 0; i < interactiveData.length; i++) {
//            interactiveLayers[i] = new InteractiveLayout(getContext()) {
//                @Override
//                public boolean onTouchEvent(MotionEvent event) {
////                        Rect bounds = new Rect();
//                    if (interactiveLayers != null) {
//
//                        int[] coordinates = new int[2];
//                        int[] coordinates2 = new int[2];
//                        int currentIndex = (int) getTag();
//                        interactiveLayers[currentIndex].getLocationOnScreen(coordinates);
//                        Rect bounds = new Rect(coordinates[0], coordinates[1] - ReaderView.coordinates[1], coordinates[0] + interactiveLayers[currentIndex].getWidth(), coordinates[1] + interactiveLayers[currentIndex].getHeight());
//                        System.out.println("Kelvyn:detectAction->" + (event.getAction() & MotionEvent.ACTION_MASK));
//                        System.out.println("Kelvyn:hitRect1->" + coordinates[0] + ", " + coordinates[1]);
//                        System.out.println("Kelvyn:hitRect2->" + event.getX() + ", " + event.getY());
//                        System.out.println("(1)Kelvyn:hitRect3->" + bounds.contains((int) event.getX(), (int) event.getY()));
//                        System.out.println("Kelvyn:hitRect4->" + ReaderView.coordinates[1]);
//                        if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
//                            if (bounds.contains((int) event.getX(), (int) event.getY())) {
//                                childChecker = false;
//                                parentChecker = false;
////                                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
////                                builder1.setMessage("Interactive clicked url:\n" + interactiveData[currentIndex].getInterUrl());
////                                builder1.setCancelable(true);
////                                builder1.setPositiveButton("OK", new DialogInterface.OnClickListener() {
////                                    public void onClick(DialogInterface dialog, int id) {
////                                        dialog.cancel();
////                                    }
////                                });
////                                AlertDialog alert11 = builder1.create();
////                                alert11.show();
//                                startIntent(interactiveData[currentIndex].getInterType(), interactiveData[currentIndex].getInterUrl());
//
//                            }
//                        }
//                    }
//                    return false;
//                }
//            };
//            interactiveLayers[i].setTag(i);
//            interactiveLayers[i].setBackgroundColor(0x7700CCFF);
//            addView(interactiveLayers[i]);
//        }
//
//        if (mCore.getDisplayPages() == 2) {
//            if (mPageNumber == 0) {
//                interactiveData2 = mCore.getInteractive(mPageNumber + 1);
//            } else {
//                interactiveData2 = mCore.getInteractive((mPageNumber * 2) + 1);
//            }
//
//            interactiveLayers2 = new InteractiveLayout[interactiveData2.length];
//
//            for (int i = 0; i < interactiveData2.length; i++) {
//                interactiveLayers2[i] = new InteractiveLayout(getContext()) {
//                    @Override
//                    public boolean onTouchEvent(MotionEvent event) {
////                        Rect bounds = new Rect();
//                        if (interactiveLayers2 != null) {
//                            int[] coordinates = new int[2];
//                            int currentIndex = (int) getTag();
//                            interactiveLayers2[currentIndex].getLocationOnScreen(coordinates);
//                            Rect bounds = new Rect(coordinates[0], coordinates[1] - ReaderView.coordinates[1], coordinates[0] + interactiveLayers2[currentIndex].getWidth(), coordinates[1] + interactiveLayers2[currentIndex].getHeight());
//                            System.out.println("Kelvyn:detectAction->" + (event.getAction() & MotionEvent.ACTION_MASK));
//                            System.out.println("Kelvyn:hitRect1->" + coordinates[0] + ", " + coordinates[1]);
//                            System.out.println("Kelvyn:hitRect2->" + event.getX() + ", " + event.getY());
//                            System.out.println("(2)Kelvyn:hitRect3->" + bounds.contains((int) event.getX(), (int) event.getY()));
//                            System.out.println("Kelvyn:hitRect4->" + ReaderView.coordinates[1]);
//                            if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
//                                if (bounds.contains((int) event.getX(), (int) event.getY())) {
////                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
////                                    builder1.setMessage("Interactive clicked url:\n" + interactiveData2[currentIndex].getInterUrl());
////                                    builder1.setCancelable(true);
////                                    builder1.setPositiveButton("OK", new DialogInterface.OnClickListener() {
////                                        public void onClick(DialogInterface dialog, int id) {
////                                            dialog.cancel();
////                                        }
////                                    });
////                                    AlertDialog alert11 = builder1.create();
////                                    alert11.show();
//                                    startIntent(interactiveData2[currentIndex].getInterType(), interactiveData2[currentIndex].getInterUrl());
//                                }
//                            }
//                        }
//                        return false;
//                    }
//                };
//                interactiveLayers2[i].setTag(i);
//                interactiveLayers2[i].setBackgroundColor(0x7700CCFF);
//                addView(interactiveLayers2[i]);
//            }
//        }
//    }
//
//    public void disableInteractive() {
//        interactiveEnabled = false;
//        System.out.println("Kelvyn:check Parent disable->" + getChildCount());
//        for (int i = 0; i < getChildCount(); i++) {
//            if (getChildAt(i) instanceof InteractiveLayout) {
//                removeView(getChildAt(i));
//            }
//        }
//    }
//
//    public void enableInteractive() {
//        interactiveEnabled = true;
//    }
//
//}
