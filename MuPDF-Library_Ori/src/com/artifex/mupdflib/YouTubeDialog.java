package com.artifex.mupdflib;

import com.artifex.mupdfdemo.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

public class YouTubeDialog extends DialogFragment implements
		YouTubePlayer.OnInitializedListener {
	public static final String YOUTUBE_BUNDLE_KEY = "data";
	private YouTubePlayerView playerView;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		View rootView = LayoutInflater.from(getActivity()).inflate(
				R.layout.youtube_webview, null);
		playerView = (YouTubePlayerView) rootView.findViewById(R.id.player);
		playerView.initialize("AIzaSyBI48nb_fFtZo0Mmi53GDJDE3NtHVQUJ8o", this);
		return rootView;
	}

	@Override
	public void onInitializationSuccess(YouTubePlayer.Provider arg0,
			YouTubePlayer player, boolean wasRestored) {
		// TODO Auto-generated method stub
		// Specify that we want to handle fullscreen behavior ourselves.
		player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI);
		if (!wasRestored) {
			String url = getArguments().getString(YOUTUBE_BUNDLE_KEY);
			String youtubeVideoID = url.substring(url.lastIndexOf("/") + 1,
					url.length());
			Toast.makeText(getActivity(), youtubeVideoID, Toast.LENGTH_SHORT)
					.show();
			player.cueVideo(youtubeVideoID);
		}
	}

	@Override
	public void onInitializationFailure(YouTubePlayer.Provider arg0,
			YouTubeInitializationResult arg1) {
		// TODO Auto-generated method stub

	}

}
