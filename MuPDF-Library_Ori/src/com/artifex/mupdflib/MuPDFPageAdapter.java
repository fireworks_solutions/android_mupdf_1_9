package com.artifex.mupdflib;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class MuPDFPageAdapter extends BaseAdapter {
	private static final String TAG = "MuPDF_PAGE_ADAPTER";
	private final Context mContext;
	private final FilePicker.FilePickerSupport mFilePickerSupport;
	private final MuPDFCore mCore;
	private final SparseArray<PointF> mPageSizes = new SparseArray<PointF>();
	private Bitmap mSharedHqBm;

	public MuPDFPageAdapter(Context c,
			FilePicker.FilePickerSupport filePickerSupport, MuPDFCore core) {

		mContext = c;
		mFilePickerSupport = filePickerSupport;
		mCore = core;
	}

	public int getCount() {
		return mCore.countPages();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		final MuPDFPageView pageView;
		System.out.println("Kelvyn:check parent->"
				+ ((MuPDFReaderView) parent).getDisplayedViewIndex());

		if (mSharedHqBm == null || mSharedHqBm.getWidth() != parent.getWidth()
				|| mSharedHqBm.getHeight() != parent.getHeight()) {
			mSharedHqBm = Bitmap.createBitmap(parent.getWidth(),
					parent.getHeight(), Bitmap.Config.ARGB_8888);
		}

		Log.d(TAG, "Kelvyn:convertView == null  position=" + position
				+ convertView);
		pageView = new MuPDFPageView(mContext, mFilePickerSupport, mCore,
				new Point(parent.getWidth(), parent.getHeight()), mSharedHqBm);

		Log.d(TAG, "pageView" + pageView);
		// if (pageView.isInteractiveEnabled()) {
		// pageView.enableInteractive();
		// } else {
		// pageView.disableInteractive();
		// }

		final PointF pageSize = mPageSizes.get(position);
		if (pageSize != null) {
			// We already know the page size. Set it up
			// immediately
			Log.d(TAG, "pageSize != null" + " set page size from array=> "
					+ pageSize.x + " " + pageSize.y);

			pageView.setPage(position, pageSize);
		} else {
			// Page size as yet unknown. Blank it for now, and
			// start a background task to find the size
			Log.d(TAG, "pageSize == null");
			pageView.blank(position);
			final AsyncTask<Void, Void, PointF> sizingTask = new AsyncTask<Void, Void, PointF>() {
				@Override
				protected PointF doInBackground(Void... arg0) {
					Log.d(TAG, "sizingTask is runing");
					PointF output = null;
					try {
						output = mCore.getPageSize(position);
					} catch (Exception e) {
						e.printStackTrace();
						output = null;
						Log.d(TAG, "failed get page size; output ->" + output);
					}
					return output;
				}

				@Override
				protected void onPostExecute(PointF result) {
					super.onPostExecute(result);
					// We now know the page size
					Log.d(TAG, "sizingTask result->" + result);
					mPageSizes.put(position, result);

					// Check that this view hasn't been reused for
					// another page since we started

					if (pageView.getPage() == position) {
						Log.d(TAG, "set new page size=> " + result.x + " "
								+ result.y);
						pageView.setPage(position, result);
					}
				}
			};
			sizingTask.execute((Void) null);
		}
		return pageView;
	}

	boolean startAsyncTask = false;
}
