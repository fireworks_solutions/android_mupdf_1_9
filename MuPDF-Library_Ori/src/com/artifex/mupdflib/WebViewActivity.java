package com.artifex.mupdflib;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import com.artifex.mupdfdemo.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

/**
 * Created by fireworks on 8/12/15.
 */
public class WebViewActivity extends YouTubeBaseActivity implements
		YouTubePlayer.OnInitializedListener {
	private YouTubePlayerView playerView;
	private YouTubePlayer player;
	public static final String YOUTUBE_BUNDLE_KEY = "data";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initWindow();
		playerView = (YouTubePlayerView) findViewById(R.id.player);
		playerView.initialize("AIzaSyBI48nb_fFtZo0Mmi53GDJDE3NtHVQUJ8o", this);

	}

	private void initWindow() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.youtube_webview);
		setFinishOnTouchOutside(false);
		getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, 0);
		android.view.WindowManager.LayoutParams params = getWindow()
				.getAttributes();
		int screenSize = getResources().getConfiguration().screenLayout
				& Configuration.SCREENLAYOUT_SIZE_MASK;
		switch (screenSize) {
		case Configuration.SCREENLAYOUT_SIZE_NORMAL:
		case Configuration.SCREENLAYOUT_SIZE_SMALL:
			params.width = LayoutParams.FILL_PARENT;
			params.height = LayoutParams.WRAP_CONTENT;

			break;
		default:
			// tablet layout
			DisplayMetrics displaymetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
			int height = displaymetrics.heightPixels;
			int width = displaymetrics.widthPixels;
			params.width = width * 3 / 4;
			params.height = LayoutParams.WRAP_CONTENT;
			break;
		}
		getWindow().setAttributes(params);

	}

	@Override
	public void onInitializationSuccess(YouTubePlayer.Provider arg0,
			YouTubePlayer player, boolean wasRestored) {
		// TODO Auto-generated method stub
		this.player = player;
		// Specify that we want to handle fullscreen behavior ourselves.
		player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI);
		if (!wasRestored) {
			String url = getIntent().getStringExtra(YOUTUBE_BUNDLE_KEY);
			String youtubeVideoID = url.substring(url.lastIndexOf("/") + 1,
					url.length());
			Toast.makeText(this, youtubeVideoID, Toast.LENGTH_SHORT).show();
			player.cueVideo(youtubeVideoID);
		}
	}

	@Override
	public void onInitializationFailure(YouTubePlayer.Provider arg0,
			YouTubeInitializationResult arg1) {
		// TODO Auto-generated method stub

	}
}