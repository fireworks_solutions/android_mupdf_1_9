package com.artifex.mupdflib;

import android.graphics.*;
import android.graphics.PorterDuff.Mode;
import android.content.Context;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import com.artifex.mupdfdemo.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

public class MuPDFCore {
	private static final String TAG = "MuPDFCore";

	/* load our native library */
	static {
		System.loadLibrary("mupdf");
	}

	/* Readable members */
	private int numPages = -1;
	private int displayPages = 1;
	private float pageWidth;
	private float pageHeight;
	private String mFileName;
	private long globals;
	private byte fileBuffer[];
	private String file_format;
	private Paint paint;
	private boolean isUnencryptedPDF;
	private HashMap<String, InteractiveData[]> interactivesArray;
	private Canvas canvas = null;

	/* The native functions */
	private native long openFile(String filename);

	private native long openBuffer();

	private native String fileFormatInternal();

	private native int countPagesInternal();

	private native void gotoPageInternal(int localActionPageNum);

	private native float getPageWidth();

	private native float getPageHeight();

	private native void drawPage(Bitmap bitmap, int pageW, int pageH,
			int patchX, int patchY, int patchW, int patchH);

	private native void updatePageInternal(Bitmap bitmap, int page, int pageW,
			int pageH, int patchX, int patchY, int patchW, int patchH);

	private native RectF[] searchPage(String text);

	private native TextChar[][][][] text();

	private native byte[] textAsHtml();

	private native void addMarkupAnnotationInternal(PointF[] quadPoints,
			int type);

	private native void addInkAnnotationInternal(PointF[][] arcs);

	private native void deleteAnnotationInternal(int annot_index);

	private native int passClickEventInternal(int page, float x, float y);

	private native void setFocusedWidgetChoiceSelectedInternal(String[] selected);

	private native String[] getFocusedWidgetChoiceSelected();

	private native String[] getFocusedWidgetChoiceOptions();

	private native int getFocusedWidgetSignatureState();

	private native String checkFocusedSignatureInternal();

	private native boolean signFocusedSignatureInternal(String keyFile,
			String password);

	private native int setFocusedWidgetTextInternal(String text);

	private native String getFocusedWidgetTextInternal();

	private native int getFocusedWidgetTypeInternal();

	private native LinkInfo[] getPageLinksInternal(int page);

	private native RectF[] getWidgetAreasInternal(int page);

	private native Annotation[] getAnnotationsInternal(int page);

	private native OutlineItem[] getOutlineInternal();

	private native boolean hasOutlineInternal();

	private native boolean needsPasswordInternal();

	private native boolean authenticatePasswordInternal(String password);

	private native MuPDFAlertInternal waitForAlertInternal();

	private native void replyToAlertInternal(MuPDFAlertInternal alert);

	private native void startAlertsInternal();

	private native void stopAlertsInternal();

	private native void destroying();

	private native boolean hasChangesInternal();

	private native void saveInternal();

	public static native boolean javascriptSupported();

	public class Cookie {
		private final long cookiePtr;

		public Cookie() {
			cookiePtr = 0;
			// cookiePtr = createCookie();
			// if (cookiePtr == 0)
			// throw new OutOfMemoryError();
		}

		public void abort() {
			// abortCookie(cookiePtr);
		}

		public void destroy() {
			// We could do this in finalize, but there's no guarantee that
			// a finalize will occur before the muPDF context occurs.
			// destroyCookie(cookiePtr);
		}
	}

	public enum WidgetType {
		NONE, TEXT, LISTBOX, COMBOBOX, SIGNATURE
	}

	private Bitmap rightBm = null, leftBm = null;

	public MuPDFCore(Context context, String filename, String xmlStrings)
			throws Exception {
		mFileName = filename;
		globals = openFile(filename);
		if (globals == 0) {
			throw new Exception(
					String.format(
							context.getString(R.string.cannot_open_file_Path),
							filename));
		}
		file_format = fileFormatInternal();
		paint = new Paint(Paint.FILTER_BITMAP_FLAG);
		if (xmlStrings != null) {
			prepareInteractive(xmlStrings);
		}
	}

	public String getFileName() {
		return mFileName;
	}

	public String getFileDirectory() {
		return (new File(getFileName())).getParent();
	}

	// public MuPDFCore(Context context, byte buffer[]) throws Exception {
	public MuPDFCore(Context context, byte buffer[], String xmlStrings)
			throws Exception {

		fileBuffer = buffer;
		// globals = openBuffer();
		globals = openBuffer();

		if (globals == 0) {
			throw new Exception(context.getString(R.string.cannot_open_buffer));
		}
		file_format = fileFormatInternal();
		paint = new Paint(Paint.FILTER_BITMAP_FLAG);
		if (xmlStrings != null) {
			prepareInteractive(xmlStrings);
		}
	}

	public int countPages() {
		if (numPages < 0)
			numPages = countPagesSynchronized();

		if (displayPages == 1)
			return numPages;
		if (numPages % 2 == 0) {
			return numPages / 2 + 1;
		}
		int toReturn = numPages / 2;
		return toReturn + 1;
	}

	public String fileFormat() {
		return file_format;
	}

	// public boolean isUnencryptedPDF() {
	// return isUnencryptedPDF;
	// }

	private synchronized int countPagesSynchronized() {
		return countPagesInternal();
	}

	/* Shim function */
	private void gotoPage(int page) {
		Log.d(TAG, "gotoPage");
		if (page > numPages - 1)
			page = numPages - 1;
		else if (page < 0)
			page = 0;
		gotoPageInternal(page);
		this.pageWidth = getPageWidth();
		this.pageHeight = getPageHeight();
	}

	public synchronized PointF getPageSize(int page) {
		Log.d(TAG, "getPageSize");
		if (displayPages == 1) {
			gotoPage(page);
			return new PointF(pageWidth, pageHeight);
		} else {
			gotoPage(page);
			if (page == numPages - 1 || page == 0) {
				// last page
				return new PointF(pageWidth * 2, pageHeight);
			}
			float leftWidth = pageWidth;
			float leftHeight = pageHeight;
			gotoPage(page + 1);
			float screenWidth = leftWidth + pageWidth;
			float screenHeight = Math.max(leftHeight, pageHeight);
			return new PointF(screenWidth, screenHeight);
		}
	}

	public MuPDFAlert waitForAlert() {
		MuPDFAlertInternal alert = waitForAlertInternal();
		return alert != null ? alert.toAlert() : null;
	}

	public void replyToAlert(MuPDFAlert alert) {
		replyToAlertInternal(new MuPDFAlertInternal(alert));
	}

	public void stopAlerts() {
		stopAlertsInternal();
	}

	public void startAlerts() {
		startAlertsInternal();
	}

	public synchronized void onDestroy() {
		destroying();
		globals = 0;
	}

	public synchronized PointF getSinglePageSize(int page) {
		gotoPage(page);
		return new PointF(pageWidth, pageHeight);
	}

	public synchronized void drawPageSynchrinized(int page, Bitmap bitmap,
			int pageW, int pageH, int patchX, int patchY, int patchW, int patchH) {
		gotoPage(page);
		// /Log.d(TAG,"drawPageSynchrinized page:"+page);
		drawPage(bitmap, pageW, pageH, patchX, patchY, patchW, patchH);
	}

	public synchronized void drawSinglePage(int page, Bitmap bitmap, int pageW,
			int pageH) {
		drawPageSynchrinized(page, bitmap, pageW, pageH, 0, 0, pageW, pageH);
	}

	public synchronized void drawPage(Bitmap bm, int page, int pageW,
			int pageH, int patchX, int patchY, int patchW, int patchH) {
		// /gotoPage(page);
		// /Bitmap bm = Bitmap.createBitmap(patchW, patchH, Config.ARGB_8888);
		// /drawPage(bm, pageW, pageH, patchX, patchY, patchW, patchH);
		// /return bm;
		canvas = null;
		// Bitmap bitmap = null;
		System.out.println("Kelvyn:DrawPage->" + page);
		try {
			// bitmap = Bitmap.createBitmap(patchW, patchH, Config.ARGB_8888);
			canvas = new Canvas(bm);
			// canvas.drawColor(0xffaaaaaa);
			// /Log.d(TAG, "drawPage "+page);

			// /Log.d(TAG,"canvas: "+canvas);

			if (displayPages == 1) {
				gotoPage(page);
				drawPage(bm, pageW, pageH, patchX, patchY, patchW, patchH);
				// return bitmap;
			} else {
				final int drawPage = (page == 0) ? 0 : page * 2 - 1;
				int leftPageW = pageW / 2;
				int rightPageW = pageW - leftPageW;

				// If patch overlaps both bitmaps (left and right) - return the
				// width of overlapping left bitpam part of the patch
				// or return full patch width if it's fully inside left bitmap
				int leftBmWidth = Math.min(leftPageW, leftPageW - patchX);

				// set left Bitmap width to zero if patch is fully overlay right
				// Bitmap
				leftBmWidth = (leftBmWidth < 0) ? 0 : leftBmWidth;

				// set the right part of the patch width, as a rest of the patch
				int rightBmWidth = patchW - leftBmWidth;

				if (drawPage == numPages - 1) {
					// draw only left page
					// canvas.drawColor(0xffaaaaaa);
					canvas.drawColor(Color.argb(255, 170, 170, 170), Mode.CLEAR);
					if (leftBmWidth > 0) {
						// canvas = new Canvas(bm);
						leftBm = Bitmap.createBitmap(leftBmWidth, patchH,
								getBitmapConfig());
						gotoPage(drawPage);
						drawPage(leftBm, leftPageW, pageH, patchX, patchY,
								leftBmWidth, patchH);
						canvas.drawBitmap(leftBm, 0, 0, paint);
						leftBm.recycle();
					}
					// if (rightBmWidth > 0) {
					// canvas.drawColor(Color.rgb(170, 170, 170));
					// rightBm = Bitmap.createBitmap(rightBmWidth,
					// patchH, getBitmapConfig());
					//
					// canvas.drawBitmap(rightBm, (float) leftBmWidth, 0,
					// paint);
					// rightBm.recycle();
					// }
				} else if (drawPage == 0) {
					// draw only right page
					//
					if (leftBmWidth > 0) {
						canvas.drawColor(Color.rgb(170, 170, 170), Mode.CLEAR);
						leftBm = Bitmap.createBitmap(leftBmWidth, patchH,
								getBitmapConfig());
						canvas.drawBitmap(leftBm, 0, 0, paint);
						leftBm.recycle();
					}
					if (rightBmWidth > 0) {

						rightBm = Bitmap.createBitmap(rightBmWidth, patchH,
								getBitmapConfig());
						gotoPage(drawPage);
						drawPage(rightBm, rightPageW, pageH,
								(leftBmWidth == 0) ? patchX - leftPageW : 0,
								patchY, rightBmWidth, patchH);

						canvas.drawBitmap(rightBm, (float) leftBmWidth, 0,
								paint);
						rightBm.recycle();
					}
				} else {
					// Need to draw two pages one by one: left and right
					Log.d("bitmap width", "" + bm.getWidth());
					canvas = new Canvas(bm);
					canvas.drawColor(Color.TRANSPARENT, Mode.CLEAR);
					// canvas.drawColor(Color.TRANSPARENT);
					// Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
					if (leftBmWidth > 0) {
						leftBm = Bitmap.createBitmap(leftBmWidth, patchH,
								getBitmapConfig());
						gotoPage(drawPage);
						drawPage(leftBm, leftPageW, pageH, patchX, patchY,
								leftBmWidth, patchH);
						canvas.drawBitmap(leftBm, 0, 0, paint);
						leftBm.recycle();
					}
					if (rightBmWidth > 0) {
						rightBm = Bitmap.createBitmap(rightBmWidth, patchH,
								getBitmapConfig());
						gotoPage(drawPage + 1);
						drawPage(rightBm, rightPageW, pageH,
								(leftBmWidth == 0) ? patchX - leftPageW : 0,
								patchY, rightBmWidth, patchH);

						canvas.drawBitmap(rightBm, (float) leftBmWidth, 0,
								paint);
						rightBm.recycle();
					}

				}
				// return bitmap;
			}
		} catch (OutOfMemoryError e) {
			// /Log.e(TAG, "draw page " + page + "failed", e);
			if (canvas != null)
				canvas.drawColor(Color.TRANSPARENT, Mode.CLEAR);
			// return bitmap;
		} catch (Exception e) {
			// TODO: handle exception
			Log.e(TAG, "update page " + page + "failed", e);
		}
	}

	public synchronized void updatePage(Bitmap bm, int page, int pageW,
			int pageH, int patchX, int patchY, int patchW, int patchH) {

		System.out.println("Kelvyn:updatePage->" + page);

		Canvas canvas = null;

		try {
			canvas = new Canvas(bm);
			// canvas.drawColor(0xffaaaaaa);
			// /Log.d(TAG,"canvas: "+canvas);
			if (displayPages == 1) {

				updatePageInternal(bm, page, pageW, pageH, patchX, patchY,
						patchW, patchH);
				// return bitmap;
			} else {
				page = (page == 0) ? 0 : page * 2 - 1;
				int leftPageW = pageW / 2;
				int rightPageW = pageW - leftPageW;

				// If patch overlaps both bitmaps (left and right) - return the
				// width of overlapping left bitpam part of the patch
				// or return full patch width if it's fully inside left bitmap
				int leftBmWidth = Math.min(leftPageW, leftPageW - patchX);

				// set left Bitmap width to zero if patch is fully overlay right
				// Bitmap
				leftBmWidth = (leftBmWidth < 0) ? 0 : leftBmWidth;

				// set the right part of the patch width, as a rest of the patch
				int rightBmWidth = patchW - leftBmWidth;
				if (page == numPages - 1) {
					// draw only left page
					// canvas.drawColor(Color.Transparent);
					canvas.drawColor(Color.argb(255, 170, 170, 170), Mode.CLEAR);
					if (leftBmWidth > 0) {
						leftBm = Bitmap.createBitmap(bm, 0, 0,
								(leftBmWidth < bm.getWidth()) ? leftBmWidth
										: bm.getWidth(), patchH);
						updatePageInternal(leftBm, page, leftPageW, pageH,
								patchX, patchY, leftBmWidth, patchH);
						canvas.drawBitmap(leftBm, 0, 0, paint);
						leftBm.recycle();
					}
					// if (rightBmWidth > 0) {
					// rightBm = Bitmap.createBitmap(bm, leftBmWidth, 0,
					// rightBmWidth,
					// patchH);
					// updatePageInternal(rightBm, page, rightPageW, pageH,
					// (leftBmWidth == 0) ? patchX - leftPageW : 0,
					// patchY, rightBmWidth, patchH);
					//
					// canvas.drawBitmap(rightBm, (float) leftBmWidth, 0,
					// paint);
					// rightBm.recycle();
					// }
				} else if (page == 0) {
					// draw only right page
					// canvas.drawColor(Color.Transparent);
					if (leftBmWidth > 0) {
						leftBm = Bitmap.createBitmap(bm, 0, 0,
								(leftBmWidth < bm.getWidth()) ? leftBmWidth
										: bm.getWidth(), patchH);
						updatePageInternal(leftBm, page, leftPageW, pageH,
								patchX, patchY, leftBmWidth, patchH);
						canvas.drawBitmap(leftBm, 0, 0, paint);
						leftBm.recycle();
					}
					if (rightBmWidth > 0) {
						rightBm = Bitmap.createBitmap(bm, leftBmWidth, 0,
								rightBmWidth, patchH);
						updatePageInternal(rightBm, page, rightPageW, pageH,
								(leftBmWidth == 0) ? patchX - leftPageW : 0,
								patchY, rightBmWidth, patchH);

						canvas.drawBitmap(rightBm, (float) leftBmWidth, 0,
								paint);
						rightBm.recycle();
					}
				} else {
					// Need to draw two pages one by one: left and right
					Log.d("bitmap width", "" + bm.getWidth());
					canvas.drawColor(Color.TRANSPARENT, Mode.CLEAR);

					if (leftBmWidth > 0) {
						leftBm = Bitmap.createBitmap(bm, 0, 0,
								(leftBmWidth < bm.getWidth()) ? leftBmWidth
										: bm.getWidth(), patchH);
						updatePageInternal(leftBm, page, leftPageW, pageH,
								patchX, patchY, leftBmWidth, patchH);
						canvas.drawBitmap(leftBm, 0, 0, paint);
						leftBm.recycle();
					}
					if (rightBmWidth > 0) {
						rightBm = Bitmap.createBitmap(bm, leftBmWidth, 0,
								rightBmWidth, patchH);
						updatePageInternal(rightBm, page, rightPageW, pageH,
								(leftBmWidth == 0) ? patchX - leftPageW : 0,
								patchY, rightBmWidth, patchH);

						canvas.drawBitmap(rightBm, (float) leftBmWidth, 0,
								paint);
						rightBm.recycle();
					}

				}
				// return bitmap;
			}
		} catch (OutOfMemoryError e) {
			// /Log.e(TAG, "update page " + page + "failed", e);
			if (canvas != null)
				canvas.drawColor(Color.TRANSPARENT, Mode.CLEAR);
			// return bitmap;
		} catch (Exception e) {
			// TODO: handle exception
			Log.e(TAG, "update page " + page + "failed", e);
		}
	}

	public synchronized PassClickResult passClickEvent(int page, float x,
			float y) {
		boolean changed = passClickEventInternal(page, x, y) != 0;

		switch (WidgetType.values()[getFocusedWidgetTypeInternal()]) {
		case TEXT:
			return new PassClickResultText(changed,
					getFocusedWidgetTextInternal());
		case LISTBOX:
		case COMBOBOX:
			return new PassClickResultChoice(changed,
					getFocusedWidgetChoiceOptions(),
					getFocusedWidgetChoiceSelected());
		case SIGNATURE:
			return new PassClickResultSignature(changed,
					getFocusedWidgetSignatureState());
		default:
			return new PassClickResult(changed);
		}

	}

	public synchronized boolean setFocusedWidgetText(int page, String text) {
		boolean success;
		gotoPage(page);
		success = setFocusedWidgetTextInternal(text) != 0 ? true : false;

		return success;
	}

	public synchronized void setFocusedWidgetChoiceSelected(String[] selected) {
		setFocusedWidgetChoiceSelectedInternal(selected);
	}

	public synchronized String checkFocusedSignature() {
		return checkFocusedSignatureInternal();
	}

	public synchronized boolean signFocusedSignature(String keyFile,
			String password) {
		return signFocusedSignatureInternal(keyFile, password);
	}

	public synchronized LinkInfo[] getPageLinks(int page) {
		// /return getPageLinksInternal(page);
		if (displayPages == 1)
			return getPageLinksInternal(page);
		LinkInfo[] leftPageLinkInfo = new LinkInfo[0];
		LinkInfo[] rightPageLinkInfo = new LinkInfo[0];
		LinkInfo[] combinedLinkInfo;
		int combinedSize = 0;
		int rightPage = page * 2;
		int leftPage = rightPage - 1;
		int count = countPages() * 2;
		if (leftPage > 0) {
			LinkInfo[] leftPageLinkInfoInternal = getPageLinksInternal(leftPage);
			if (null != leftPageLinkInfoInternal) {
				leftPageLinkInfo = leftPageLinkInfoInternal;
				combinedSize += leftPageLinkInfo.length;
			}
		}
		if (rightPage < count) {
			LinkInfo[] rightPageLinkInfoInternal = getPageLinksInternal(rightPage);
			if (null != rightPageLinkInfoInternal) {
				rightPageLinkInfo = rightPageLinkInfoInternal;
				combinedSize += rightPageLinkInfo.length;
			}
		}

		combinedLinkInfo = new LinkInfo[combinedSize];
		for (int i = 0; i < leftPageLinkInfo.length; i++) {
			combinedLinkInfo[i] = leftPageLinkInfo[i];
		}

		LinkInfo temp;
		for (int i = 0, j = leftPageLinkInfo.length; i < rightPageLinkInfo.length; i++, j++) {
			temp = rightPageLinkInfo[i];
			temp.rect.left += pageWidth;
			temp.rect.right += pageWidth;
			combinedLinkInfo[j] = temp;
		}

		return combinedLinkInfo;
	}

	public synchronized RectF[] getWidgetAreas(int page) {
		return getWidgetAreasInternal(page);
	}

	public synchronized Annotation[] getAnnoations(int page) {
		return getAnnotationsInternal(page);
	}

	public synchronized RectF[] searchPage(int page, String text) {
		gotoPage(page);
		return searchPage(text);
	}

	public synchronized byte[] html(int page) {
		gotoPage(page);
		return textAsHtml();
	}

	public synchronized TextWord[][] textLines(int page) {
		gotoPage(page);
		TextChar[][][][] chars = text();

		// The text of the page held in a hierarchy (blocks, lines, spans).
		// Currently we don't need to distinguish the blocks level or
		// the spans, and we need to collect the text into words.
		ArrayList<TextWord[]> lns = new ArrayList<TextWord[]>();

		for (TextChar[][][] bl : chars) {
			if (bl == null)
				continue;
			for (TextChar[][] ln : bl) {
				ArrayList<TextWord> wds = new ArrayList<TextWord>();
				TextWord wd = new TextWord();

				for (TextChar[] sp : ln) {
					for (TextChar tc : sp) {
						if (tc.c != ' ') {
							wd.Add(tc);
						} else if (wd.w.length() > 0) {
							wds.add(wd);
							wd = new TextWord();
						}
					}
				}

				if (wd.w.length() > 0)
					wds.add(wd);

				if (wds.size() > 0)
					lns.add(wds.toArray(new TextWord[wds.size()]));
			}
		}

		return lns.toArray(new TextWord[lns.size()][]);
	}

	public synchronized void addMarkupAnnotation(int page, PointF[] quadPoints,
			Annotation.Type type) {
		gotoPage(page);
		addMarkupAnnotationInternal(quadPoints, type.ordinal());
	}

	public synchronized void addInkAnnotation(int page, PointF[][] arcs) {
		gotoPage(page);
		addInkAnnotationInternal(arcs);
	}

	public synchronized void deleteAnnotation(int page, int annot_index) {
		gotoPage(page);
		deleteAnnotationInternal(annot_index);
	}

	public synchronized boolean hasOutline() {
		return hasOutlineInternal();
	}

	public synchronized OutlineItem[] getOutline() {
		return getOutlineInternal();
	}

	public synchronized boolean needsPassword() {
		return needsPasswordInternal();
	}

	public synchronized boolean authenticatePassword(String password) {
		return authenticatePasswordInternal(password);
	}

	public synchronized boolean hasChanges() {
		return hasChangesInternal();
	}

	public synchronized void save() {
		saveInternal();
	}

	public int getDisplayPages() {
		return displayPages;
	}

	private Config getBitmapConfig() {
		return Config.ARGB_8888;
	}

	public int countDisplays() {
		int pages = countPages();
		if (pages % 2 == 0) {
			return pages / 2 + 1;
		} else
			return pages / 2;
	}

	public void setDisplayPages(int pages) throws IllegalStateException {
		if (pages <= 0 || pages > 2) {
			throw new IllegalStateException(
					"MuPDFCore can only handle 1 or 2 pages per screen!");
		}
		displayPages = pages;
	}

	/**
	 * @return
	 */
	public int countSinglePages() {
		return numPages;
	}

	public InteractiveData[] getInteractive(int page) {
		System.out.println("Kelvyn:Getting interactive for page-> " + page);
		if (interactivesArray != null
				&& interactivesArray.containsKey(page + "")) {
			return interactivesArray.get(page + "");
		} else {
			return new InteractiveData[0];
		}
	}

	private void prepareInteractive(String xmlInput) {
		/*
		 * Hardcoded interactive array
		 */
		int[] tops = { 50, 300, 300 };
		int[] left = { 50, 50, 300 };
		int[] height = { 100, 100, 100 };
		int[] width = { 200, 50, 50 };
		String[] types = { "link", "video", "audio" };
		String[] urls = { "http://www.google.com.my", "http://www.youtube.com",
				"http://www.soundcloud.com" };

		System.out.println("Kelvyn:Xmlinput->" + xmlInput);
		ArrayList<String> htmlList = processPagesXML(xmlInput);

		interactivesArray = new HashMap<String, InteractiveData[]>();
		for (int i = 0; i < htmlList.size(); i++) {

			System.out.println("Kelvyn:xmlhtmlFilePath->" + htmlList.get(i));
			// InteractiveData[] singlePageInteractives = );
			// for(int x = 0; x<singlePageInteractives.length; x++){
			// singlePageInteractives[x] = new
			// InteractiveData(left[x],tops[x],left[x] + width[x], tops[x] +
			// height[x], types[x],urls[x]);
			// }

			String pageNumberStr = htmlList.get(i).substring(
					htmlList.get(i).lastIndexOf("/") + 1,
					htmlList.get(i).lastIndexOf("."));
			System.out.println("Kelvyn:xmlpageNumberStr->" + pageNumberStr);
			interactivesArray.put(pageNumberStr + "",
					processHTML(htmlList.get(i)));
		}
	}

	private ArrayList<String> processPagesXML(String filePath) {
		ArrayList<String> output = new ArrayList<String>();

		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();

			String xmlFilePath;
			File pagesFilePath = null;
			if (filePath.lastIndexOf("/") > 0) {

				xmlFilePath = filePath.substring(0,
						filePath.lastIndexOf("/") + 1);
				System.out.println("Kelvyn:xmlFilePath->" + xmlFilePath);
				pagesFilePath = new File(xmlFilePath);
			} else {
				return output;
			}
			File file = new File(pagesFilePath.getParentFile(), "pages.xml");
			System.out.println("Kelvyn:xmlFilePathFinal->"
					+ file.getAbsolutePath());

			BufferedReader br = new BufferedReader(new FileReader(file));

			xpp.setInput(br);
			int eventType = xpp.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				// if(eventType == XmlPullParser.START_DOCUMENT) {
				// System.out.println("Kelvyn:XMLStart document");
				// } else
				if (eventType == XmlPullParser.START_TAG) {
					System.out.println("Kelvyn:XMLStart tag " + xpp.getName());
					if (xpp.getName().equalsIgnoreCase("html")
							&& xpp.next() != XmlPullParser.END_DOCUMENT) {
						output.add(pagesFilePath.getParent() + "/"
								+ xpp.getText());
						System.out.println("Kelvyn:XMLText " + xpp.getText());
					}
				}
				// else if(eventType == XmlPullParser.END_TAG) {
				// System.out.println("Kelvyn:XMLEnd tag "+xpp.getName());
				// } else if(eventType == XmlPullParser.TEXT) {
				// System.out.println("Kelvyn:XMLText "+xpp.getText());
				// }
				eventType = xpp.next();
			}
			System.out.println("Kelvyn:XMLEnd document");
		} catch (Exception ex) {
			ex.printStackTrace();
			return output;
		}
		return output;
	}

	public InteractiveData[] processHTML(String filePath) {
		InteractiveData[] output = new InteractiveData[0];
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();

			// String xmlFilePath;
			// if(filePath.lastIndexOf("/")>0){
			// xmlFilePath = filePath.substring(0,filePath.lastIndexOf("/")+1);
			// System.out.println("Kelvyn:xmlFilePath->" + xmlFilePath);
			// }else{
			// return output;
			// }
			File file = new File(filePath);
			BufferedReader br = new BufferedReader(new FileReader(file));

			ArrayList<InteractiveData> tempArray = new ArrayList<InteractiveData>();
			xpp.setInput(br);
			int eventType = xpp.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				Bundle interactiveBundle = new Bundle();
				if (eventType == XmlPullParser.START_DOCUMENT) {
					System.out
							.println("Kelvyn:XMLStart HTML Document filePath->"
									+ filePath);
				} else if (eventType == XmlPullParser.START_TAG) {
					if (xpp.getName().equalsIgnoreCase("a")) {
						System.out.println("Kelvyn:XML href -> "
								+ xpp.getAttributeValue(null, "href"));
						File interactivePDF = new File(filePath);
						String interativePDFPath = "";
						if (xpp.getAttributeValue(null, "href").toLowerCase()
								.contains("{base}")) {
							interativePDFPath = xpp.getAttributeValue(null,
									"href").replace("{base}",
									interactivePDF.getParentFile().getParent());
						} else {
							interativePDFPath = xpp.getAttributeValue(null,
									"href");
						}

						interactiveBundle.putString(
								InteractiveData.INTER_URL_KEY,
								interativePDFPath);
						InteractiveData tempID = new InteractiveData(
								Integer.parseInt(xpp.getAttributeValue(null,
										"left")), Integer.parseInt(xpp
										.getAttributeValue(null, "top")), 0, 0,
								"a", xpp.getAttributeValue(null, "target"),
								interactiveBundle);
						int tempEventType = xpp.next();
						while (tempEventType != XmlPullParser.END_DOCUMENT
								&& !(tempEventType == XmlPullParser.END_TAG && xpp
										.getName().equalsIgnoreCase("a"))) {
							System.out.println("Kelvyn:XML inner loop -> "
									+ xpp.getAttributeValue(null, "href"));
							if (tempEventType == XmlPullParser.START_TAG
									&& xpp.getName().equalsIgnoreCase("img")) {
								tempID.setInterX2(tempID.getInterX1()
										+ Integer.parseInt(xpp
												.getAttributeValue(null,
														"width")));
								tempID.setInterY2(tempID.getInterY1()
										+ Integer.parseInt(xpp
												.getAttributeValue(null,
														"height")));

								File imagePath = new File(filePath);
								String imageNewPath = xpp.getAttributeValue(
										null, "src");
								if (!imageNewPath
										.contains("btn_transparent.png")) {
									String interativeVideoPath = imageNewPath
											.replace("{base}", imagePath
													.getParentFile()
													.getParent());
									tempID.setButtonSource(interativeVideoPath);
								}
								break;
							}
							tempEventType = xpp.next();
						}
						tempArray.add(tempID);

					} else if (xpp.getName().equalsIgnoreCase("video")) {
						int left, width, top, height;
						left = Integer.parseInt(xpp.getAttributeValue(null,
								"left"));
						width = Integer.parseInt(xpp.getAttributeValue(null,
								"left"))
								+ Integer.parseInt(xpp.getAttributeValue(null,
										"width"));
						top = Integer.parseInt(xpp.getAttributeValue(null,
								"top"));
						height = Integer.parseInt(xpp.getAttributeValue(null,
								"top"))
								+ Integer.parseInt(xpp.getAttributeValue(null,
										"height"));

						interactiveBundle.putString(
								InteractiveData.INTER_AUTO_PLAY,
								xpp.getAttributeValue(null, "autoplay"));
						InteractiveData tempID = new InteractiveData(left, top,
								width, height, "video", "normal",
								interactiveBundle);
						File imagePath = new File(filePath);
						String imageNewPath = xpp.getAttributeValue(null,
								"poster");
						if (!imageNewPath.contains("btn_transparent.png")) {
							String interativeVideoPath = imageNewPath.replace(
									"{base}", imagePath.getParentFile()
											.getParent());
							tempID.setButtonSource(interativeVideoPath);
						}
						int tempEventType = xpp.next();
						while (tempEventType != XmlPullParser.END_DOCUMENT
								&& !(tempEventType == XmlPullParser.END_TAG && xpp
										.getName().equalsIgnoreCase("video"))) {

							if (tempEventType == XmlPullParser.START_TAG
									&& xpp.getName().equalsIgnoreCase("source")) {
								File videoParentPath = new File(filePath);
								String xmlVideoPath = xpp.getAttributeValue(
										null, "src");
								String interativeVideoPath = xmlVideoPath
										.replace("{base}", videoParentPath
												.getParentFile().getParent());
								Log.v("set Video interative path : ",
										interativeVideoPath);
								interactiveBundle.putString(
										InteractiveData.INTER_URL_KEY,
										interativeVideoPath);
								tempID.setInteractiveData(interactiveBundle);
								break;
							}
							tempEventType = xpp.next();
						}

						tempArray.add(tempID);

					} else if (xpp.getName().equalsIgnoreCase("audio")) {
						interactiveBundle.putString(
								InteractiveData.INTER_AUTO_PLAY,
								xpp.getAttributeValue(null, "autoplay"));
						interactiveBundle.putString(InteractiveData.INTER_LOOP,
								xpp.getAttributeValue(null, "loop"));
						InteractiveData tempID = new InteractiveData(
								Integer.parseInt(xpp.getAttributeValue(null,
										"left")), Integer.parseInt(xpp
										.getAttributeValue(null, "top")), 0, 0,
								"audio", "normal", interactiveBundle);
						int tempEventType = xpp.next();
						while (tempEventType != XmlPullParser.END_DOCUMENT
								&& !(tempEventType == XmlPullParser.END_TAG && xpp
										.getName().equalsIgnoreCase("audio"))) {
							if (tempEventType == XmlPullParser.START_TAG
									&& xpp.getName().equalsIgnoreCase("source")) {
								File audioParentPath = new File(filePath);
								String xmlVideoPath = xpp.getAttributeValue(
										null, "src");
								String interativeAudioPath = xmlVideoPath
										.replace("{base}", audioParentPath
												.getParentFile().getParent());
								Log.v("set Audio interative path : ",
										interativeAudioPath);
								// tempID.setInterUrl(interativeAudioPath);
								interactiveBundle.putString(
										InteractiveData.INTER_URL_KEY,
										interativeAudioPath);
							}
							if (tempEventType == XmlPullParser.START_TAG
									&& xpp.getName().equalsIgnoreCase("img")) {
								tempID.setInterX2(tempID.getInterX1()
										+ Integer.parseInt(xpp
												.getAttributeValue(null,
														"width")));
								tempID.setInterY2(tempID.getInterY1()
										+ Integer.parseInt(xpp
												.getAttributeValue(null,
														"height")));

								File imagePath = new File(filePath);
								String imageNewPath = xpp.getAttributeValue(
										null, "src");
								if (!imageNewPath
										.contains("btn_transparent.png")) {
									String interativeVideoPath = imageNewPath
											.replace("{base}", imagePath
													.getParentFile()
													.getParent());
									tempID.setButtonSource(interativeVideoPath);
								}
							}
							tempEventType = xpp.next();
						}
						tempArray.add(tempID);

						// tempArray.add(new
						// InteractiveData(Integer.parseInt(xpp.getAttributeValue(null,
						// "left")),
						// Integer.parseInt(xpp.getAttributeValue(null, "top")),
						// Integer.parseInt(xpp.getAttributeValue(null, "left"))
						// + Integer.parseInt(xpp.getAttributeValue(null,
						// "width")),
						// Integer.parseInt(xpp.getAttributeValue(null, "top"))
						// + Integer.parseInt(xpp.getAttributeValue(null,
						// "height")), "youtube", xpp.getAttributeValue(null,
						// "src")));
					} else if (xpp.getName().equalsIgnoreCase("email")) {
						interactiveBundle.putString(
								InteractiveData.INTER_URL_KEY,
								xpp.getAttributeValue(null, "to"));
						interactiveBundle.putString(
								InteractiveData.INTER_EMAIL_SUBJECT_KEY,
								xpp.getAttributeValue(null, "subject"));
						InteractiveData tempID = new InteractiveData(
								Integer.parseInt(xpp.getAttributeValue(null,
										"left")), Integer.parseInt(xpp
										.getAttributeValue(null, "top")), 0, 0,
								"email", "normal", interactiveBundle);
						int tempEventType = xpp.next();
						while (tempEventType != XmlPullParser.END_DOCUMENT
								&& !(tempEventType == XmlPullParser.END_TAG && xpp
										.getName().equalsIgnoreCase("email"))) {
							if (tempEventType == XmlPullParser.START_TAG
									&& xpp.getName().equalsIgnoreCase("img")) {
								tempID.setInterX2(tempID.getInterX1()
										+ Integer.parseInt(xpp
												.getAttributeValue(null,
														"width")));
								tempID.setInterY2(tempID.getInterY1()
										+ Integer.parseInt(xpp
												.getAttributeValue(null,
														"height")));

								File imagePath = new File(filePath);
								String imageNewPath = xpp.getAttributeValue(
										null, "src");
								if (!imageNewPath
										.contains("btn_transparent.png")) {
									String interativeVideoPath = imageNewPath
											.replace("{base}", imagePath
													.getParentFile()
													.getParent());
									tempID.setButtonSource(interativeVideoPath);
								}
								break;
							}
							tempEventType = xpp.next();
						}
						tempArray.add(tempID);

					} else if (xpp.getName().equalsIgnoreCase("gallery")) {
						// tempArray.add(new
						// InteractiveData(Integer.parseInt(xpp.getAttributeValue(null,
						// "left")),
						// Integer.parseInt(xpp.getAttributeValue(null, "top")),
						// Integer.parseInt(xpp.getAttributeValue(null, "left"))
						// + Integer.parseInt(xpp.getAttributeValue(null,
						// "width")),
						// Integer.parseInt(xpp.getAttributeValue(null, "top"))
						// + Integer.parseInt(xpp.getAttributeValue(null,
						// "height")), "youtube", xpp.getAttributeValue(null,
						// "src")));
					} else if (xpp.getName().equalsIgnoreCase("webgallery")
							|| xpp.getName().equalsIgnoreCase("webform")) {
						String interactiveType = xpp.getName();
						interactiveBundle.putString(
								InteractiveData.INTER_URL_KEY,
								xpp.getAttributeValue(null, "src"));
						InteractiveData tempID = new InteractiveData(
								Integer.parseInt(xpp.getAttributeValue(null,
										"left")), Integer.parseInt(xpp
										.getAttributeValue(null, "top")),
								Integer.parseInt(xpp.getAttributeValue(null,
										"left"))
										+ Integer.parseInt(xpp
												.getAttributeValue(null,
														"width")),
								Integer.parseInt(xpp.getAttributeValue(null,
										"top"))
										+ Integer.parseInt(xpp
												.getAttributeValue(null,
														"height")),
								interactiveType, "normal", interactiveBundle);
						File imagePath = new File(filePath);
						String imageNewPath = xpp.getAttributeValue(null,
								"poster");
						if (!imageNewPath.contains("btn_transparent.png")) {
							String interativeVideoPath = imageNewPath.replace(
									"{base}", imagePath.getParentFile()
											.getParent());
							tempID.setButtonSource(interativeVideoPath);
						}
						tempArray.add(tempID);
					} else if (xpp.getName().equalsIgnoreCase("iframe")) {
						// (left[x], tops[x], left[x] + width[x], tops[x] +
						// height[x], types[x], urls[x])
						interactiveBundle.putString(
								InteractiveData.INTER_URL_KEY,
								xpp.getAttributeValue(null, "src"));
						InteractiveData tempID = new InteractiveData(
								Integer.parseInt(xpp.getAttributeValue(null,
										"left")), Integer.parseInt(xpp
										.getAttributeValue(null, "top")),
								Integer.parseInt(xpp.getAttributeValue(null,
										"left"))
										+ Integer.parseInt(xpp
												.getAttributeValue(null,
														"width")),
								Integer.parseInt(xpp.getAttributeValue(null,
										"top"))
										+ Integer.parseInt(xpp
												.getAttributeValue(null,
														"height")), "youtube",
								"normal", interactiveBundle);
						File imagePath = new File(filePath);
						String imageNewPath = xpp.getAttributeValue(null,
								"poster");
						if (!imageNewPath.contains("btn_transparent.png")) {
							String interativeVideoPath = imageNewPath.replace(
									"{base}", imagePath.getParentFile()
											.getParent());
							tempID.setButtonSource(interativeVideoPath);
						}
						tempArray.add(tempID);
					}
					System.out.println("Kelvyn:XMLStart tag " + xpp.getName());
				} else if (eventType == XmlPullParser.END_TAG) {
					System.out.println("Kelvyn:XMLEnd tag " + xpp.getName());
				} else if (eventType == XmlPullParser.TEXT) {
					System.out.println("Kelvyn:XMLText " + xpp.getText());
				}
				eventType = xpp.next();
			}
			output = new InteractiveData[tempArray.size()];
			for (int i = 0; i < tempArray.size(); i++) {
				output[i] = tempArray.get(i);
			}
			System.out.println("Kelvyn:XMLEnd document");
		} catch (Exception ex) {
			ex.printStackTrace();
			return output;
		}
		return output;
	}
}
