package com.artifex.mupdfdemo_bk;

import android.os.Bundle;
import android.view.Window;
import android.widget.Toast;

import com.artifex.mupdfdemo.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

/**
 * Created by fireworks on 8/12/15.
 */
public class WebViewActivity extends YouTubeBaseActivity implements
		YouTubePlayer.OnInitializedListener {
	private YouTubePlayerView playerView;
	private YouTubePlayer player;
	public static final String YOUTUBE_BUNDLE_KEY = "data";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.youtube_webview);
		playerView = (YouTubePlayerView) findViewById(R.id.player);
		playerView.initialize("AIzaSyBI48nb_fFtZo0Mmi53GDJDE3NtHVQUJ8o", this);

	}

	@Override
	public void onInitializationSuccess(YouTubePlayer.Provider arg0,
			YouTubePlayer player, boolean wasRestored) {
		// TODO Auto-generated method stub
		this.player = player;
		// Specify that we want to handle fullscreen behavior ourselves.
		player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI);
		if (!wasRestored) {
			String url = getIntent().getStringExtra(YOUTUBE_BUNDLE_KEY);
			String youtubeVideoID = url.substring(url.lastIndexOf("/") + 1,
					url.length());
			Toast.makeText(this, youtubeVideoID, Toast.LENGTH_SHORT).show();
			player.cueVideo(youtubeVideoID);
		}
	}

	@Override
	public void onInitializationFailure(YouTubePlayer.Provider arg0,
			YouTubeInitializationResult arg1) {
		// TODO Auto-generated method stub

	}
}