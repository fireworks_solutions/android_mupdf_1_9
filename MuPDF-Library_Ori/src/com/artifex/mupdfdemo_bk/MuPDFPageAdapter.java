package com.artifex.mupdfdemo_bk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PointF;
import android.os.Handler;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;

public class MuPDFPageAdapter extends BaseAdapter {
    private final Context mContext;
    private final FilePicker.FilePickerSupport mFilePickerSupport;
    private final MuPDFCore mCore;
    private final SparseArray<PointF> mPageSizes = new SparseArray<PointF>();
    private Bitmap mSharedHqBm;

    public MuPDFPageAdapter(Context c, FilePicker.FilePickerSupport filePickerSupport, MuPDFCore core) {

        mContext = c;
        mFilePickerSupport = filePickerSupport;
        mCore = core;
    }

    public int getCount() {
        return mCore.countPages();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final MuPDFPageView pageView;
        System.out.println("Kelvyn:check parent->" + ((MuPDFReaderView) parent).getDisplayedViewIndex());

        //if (convertView == null) {
            if (mSharedHqBm == null || mSharedHqBm.getWidth() != parent.getWidth() || mSharedHqBm.getHeight() != parent.getHeight())
                mSharedHqBm = Bitmap.createBitmap(parent.getWidth(), parent.getHeight(), Bitmap.Config.ARGB_8888);
            System.out.println("Kelvyn:convertView == null  position=" + position);
            pageView = new MuPDFPageView(mContext, mFilePickerSupport, mCore, new Point(parent.getWidth(), parent.getHeight()), mSharedHqBm);

        //} else {
        //    System.out.println("Kelvyn:convertView != null position=" + position);
        //    pageView = (MuPDFPageView) convertView;
        //}
        pageView.disableInteractive();

        final PointF pageSize = mPageSizes.get(position);
        if (pageSize != null) {
            // We already know the page size. Set it up
            // immediately
            System.out.println("Kelvyn:pageSize != null");

//            Handler h = new Handler();
//            final Runnable r2 = new Runnable() {
//
//                @Override
//                public void run() {
//                    System.out.println("Kelvyn:" + ((MuPDFReaderView) parent).getDisplayedViewIndex() + " vs " + position);
//                    if (!((MuPDFReaderView) parent).mUserInteracting) {
//                        if (position <= ((MuPDFReaderView) parent).getDisplayedViewIndex() + 1 && position >= ((MuPDFReaderView) parent).getDisplayedViewIndex() - 1) {
//                            System.out.println("Kelvyn:it is beside so render it");
//                            pageView.setPage(position, pageSize);
//                        }
//                    }
//                }
//            };
//            h.postDelayed(r2, 1500);


			pageView.setPage(position, pageSize);
        } else {
            // Page size as yet unknown. Blank it for now, and
            // start a background task to find the size
            System.out.println("Kelvyn:pageSize == null");
            pageView.blank(position);
            AsyncTask<Void, Void, PointF> sizingTask = new AsyncTask<Void, Void, PointF>() {
                @Override
                protected PointF doInBackground(Void... arg0) {
                    System.out.println("Kelvyn:pageSize==null pre-getPageSize");
                    PointF output = mCore.getPageSize(position);
                    System.out.println("Kelvyn:pageSize==null post-getPageSize");
                    return output;
                }

                @Override
                protected void onPostExecute(PointF result) {
                    super.onPostExecute(result);
                    // We now know the page size
                    mPageSizes.put(position, result);
                    // Check that this view hasn't been reused for
                    // another page since we started
                    if (pageView.getPage() == position) {
//                    if (!((MuPDFReaderView) parent).mUserInteracting) {
//                        if (position <= ((MuPDFReaderView) parent).getDisplayedViewIndex() + 1 && position >= ((MuPDFReaderView) parent).getDisplayedViewIndex() - 1) {
                            System.out.println("Kelvyn:InpageSizeasynctask==null");
                            pageView.setPage(position, result);
//                        }
                    }
                }
            };

            sizingTask.execute((Void) null);
        }
        return pageView;
    }
}
