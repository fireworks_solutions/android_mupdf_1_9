package com.artifex.mupdfdemo_bk;

public enum WidgetType {
	NONE,
	TEXT,
	LISTBOX,
	COMBOBOX,
	SIGNATURE
}
