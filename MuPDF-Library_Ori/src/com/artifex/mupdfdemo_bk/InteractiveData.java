package com.artifex.mupdfdemo_bk;

public class InteractiveData {

	private int interX1;
	private int interY1;
	private int interX2;
	private int interY2;
	private String interType;
	private String interUrl;

	public InteractiveData(int interX1, int interY1, int interX2, int interY2,
			String interType, String interUrl) {

		this.interX1 = interX1;
		this.interY1 = interY1;
		this.interX2 = interX2;
		this.interY2 = interY2;
		this.interType = interType;
		this.interUrl = interUrl;

	}

	public InteractiveData() {
	}

	public int getInterX1() {
		return interX1;
	}

	public void setInterX1(int interX1) {
		this.interX1 = interX1;
	}

	public int getInterY1() {
		return interY1;
	}

	public void setInterY1(int interY1) {
		this.interY1 = interY1;
	}

	public int getInterX2() {
		return interX2;
	}

	public void setInterX2(int interX2) {
		this.interX2 = interX2;
	}

	public int getInterY2() {
		return interY2;
	}

	public void setInterY2(int interY2) {
		this.interY2 = interY2;
	}

	public String getInterType() {
		return interType;
	}

	public void setInterType(String interType) {
		this.interType = interType;
	}

	public String getInterUrl() {
		return interUrl;
	}

	 public void setInterUrl(String interUrl) {
	 this.interUrl = interUrl;
	 }
}